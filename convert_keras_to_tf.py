# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 22:07:21 2017

@author: reich_canlas
"""
import os
import tensorflow as tf
from keras import backend as K
from keras.layers import Dropout
from keras.models import load_model, model_from_json
from tensorflow.python.framework import graph_util
from tensorflow.python.platform import gfile
from tensorflow.python.tools.optimize_for_inference_lib import node_name_from_input, node_from_map, \
    ensure_graph_is_valid

# os.environ["CUDA_VISIBLE_DEVICES"] = "6"
NUM_CLASSES = 6
# CONVLSTM_IMG_GRAPH = "results_img/chkpts/best_img_1510487176_aded_weights_combined.h5"
# CONVLSTM_ACC_GRAPH = "results_acc/chkpts/best_acc_1509110217_aded_weights_combined.h5"
# CONVLSTM_IMG_GRAPH = "results_img/chkpts/best_img_1510487176_aded_weights_combined.h5-over-new.h5"
CONVLSTM_IMG_GRAPH = "results_img/chkpts/best_img_1510487176_aded_weights_combined.h5-over-new.h5"
CONVLSTM_ACC_GRAPH = "results_acc/chkpts/best_acc_1509110217_aded_weights_combined.h5-over-new.h5"
CONVLSTM_GYR_GRAPH = "results_gyr/chkpts/best_gyr_1509114072_aded_weights_combined.h5-over-new.h5"
CONVLSTM_MAG_GRAPH = "results_mag/chkpts/best_mag_1512053475_aded_combined.h5-over-new.h5"
# CONVLSTM_FUSION_GRAPH = 'results_fusion_file/chkpts/best_fusion_file_1513656547_aded_combined.h5'
# CONVLSTM_IMG_PB = 'converted_models/lstm_image_trans.pb'
# CONVLSTM_ACC_PB = 'converted_models/lstm_accel_trans.pb'
# CONVLSTM_GYR_PB = 'converted_models/lstm_gyro_trans.pb'
# CONVLSTM_MAG_PB = 'converted_models/lstm_mag_trans.pb'

# These pb files are the best source for the individual graphs
CONVLSTM_IMG_PB = 'results_img/img.pb'
CONVLSTM_ACC_PB = 'results_acc/acc.pb'
CONVLSTM_GYR_PB = 'results_gyr/gyr.pb'
CONVLSTM_MAG_PB = 'results_mag/mag.pb'
CONVLSTM_FUSION_PB = 'results_fusion_file/chkpts/fusion_graph.pb'
# CONVLSTM_FUSION_PB = "results_fusion_file/converted_models/fusion.pb"
INCEPTION_VERSION = 1
if INCEPTION_VERSION == 1:
    # RETRAINED_GRAPH_NAME = "inception_v1_slim_frozen.pb"
    INCEPTION_GRAPH_NAME = "inception_v1_optimized.pb"
    POOL_TENSOR_NAME = 'InceptionV1/Logits/MaxPool_0a_7x7/AvgPool:0'
elif INCEPTION_VERSION == 3:
    RETRAINED_GRAPH_NAME = "inception_v3.pb"
    POOL_TENSOR_NAME = 'pool_3/_reshape:0'
elif INCEPTION_VERSION == 'M':
    RETRAINED_GRAPH_NAME = "mobilenet.pb"
    POOL_TENSOR_NAME = "MobilenetV1/Predictions/Reshape:0"

UNIFIED_PB_FILENAME = 'final_unified_pb/har.pb'

IMG_INPUT_TENSOR = "Image/lstm_input_img"
ACC_INPUT_TENSOR = "Sensor/acc/lstm_input_acc"
GYR_INPUT_TENSOR = "Sensor/gyr/lstm_input_gyr"
MAG_INPUT_TENSOR = "Sensor/mag/lstm_input_mag"
INCEPTION_INPUT_TENSOR = "InceptionGraph/div"

# IMG_FINAL_TENSOR = "img/pred_img/Softmax"
# ACC_FINAL_TENSOR = "acc/pred_acc/Softmax"
# GYR_FINAL_TENSOR = "gyr/pred_gyr/Softmax"
# MAG_FINAL_TENSOR = "mag/pred_mag/Softmax"

IMG_FINAL_TENSOR = "img/pred_img/Softmax"
ACC_FINAL_TENSOR = "acc/pred_acc/Softmax"
GYR_FINAL_TENSOR = "gyr/pred_gyr/Softmax"
MAG_FINAL_TENSOR = "mag/pred_mag/Softmax"
INCEPTION_POOL_TENSOR_NAME = 'InceptionGraph/InceptionV1/Logits/MaxPool_0a_7x7/AvgPool'

FUSION_AVE_OUTPUT_TENSOR = "Fusion/fusion_all_ave"
FUSION_WEIGHTED_AVE_OUTPUT_TENSOR = "Fusion/fusion_weighted_ave"
FUSION_OUTPUT_TENSOR = "pred_fusion/Softmax"
STANDARDIZE_ACC_OUTPUT_TENSOR = 'StandardizeSensor/acc_normalize_output'
STANDARDIZE_GYR_OUTPUT_TENSOR = 'StandardizeSensor/gyr_normalize_output'
STANDARDIZE_MAG_OUTPUT_TENSOR = 'StandardizeSensor/mag_normalize_output'


def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    """
    Freezes the state of a session into a pruned computation graph.

    Creates a new computation graph where variable nodes are replaced by
    constants taking their current value in the session. The new graph will be
    pruned so subgraphs that are not necessary to compute the requested
    outputs are removed.
    @param session The TensorFlow session to be frozen.
    @param keep_var_names A list of variable names that should not be frozen,
                          or None to freeze all the variables in the graph.
    @param output_names Names of the relevant graph outputs.
    @param clear_devices Remove the device directives from the graph for better portability.
    @return The frozen graph definition.
    """
    from tensorflow.python.framework.graph_util import convert_variables_to_constants
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = convert_variables_to_constants(session, input_graph_def,
                                                      output_names, freeze_var_names)
        return frozen_graph


def optimize_pb(input_model_path, output_model_path, final_tensor_name):
    _ = load_model(input_model_path, compile=False)
    with tf.Session() as sess:
        pruned_graph = graph_util.remove_training_nodes(sess.graph.as_graph_def())

        # Convert trained variables to constants
        output_graph = graph_util.convert_variables_to_constants(sess, pruned_graph, [final_tensor_name])
        with gfile.FastGFile(output_model_path, 'wb') as f:
            f.write(output_graph.SerializeToString())


def display_nodes(nodes):
    for i, node in enumerate(nodes):
        print('%d %s %s' % (i, node.name, node.op))
        [print(u'└─── %d ─ %s' % (i, n)) for i, n in enumerate(node.input)]


# NOTE: Not recommended to use this! Use keras training phase switch instead
def drop_dropout(temp_graph_def):
    for node in temp_graph_def.node:
        for idx, i in enumerate(node.input):
            input_clean = node_name_from_input(i)
            input_node_map = ensure_graph_is_valid(temp_graph_def)
            if input_clean.endswith('/cond/Merge') and input_clean.split('/')[-3].startswith('dropout'):
                identity = node_from_map(input_node_map, i).input[0]
                assert identity.split('/')[-1] == 'Identity'
                parent = node_from_map(input_node_map, node_from_map(input_node_map, identity).input[0])
                pred_id = parent.input[1]
                assert pred_id.split('/')[-1] == 'pred_id'
                good = parent.input[0]
                node.input[idx] = good


# NOTE: Not recommended to use this! Use keras training phase switch instead
def remove_dropout(model):
    for k in model.layers:
        if type(k) is Dropout:
            print("Found dropout layer. Removing...")
            model.layers.remove(k)

    return model


def convert_keras_to_tf_pb(input_model_path, output_model_path, final_tensor_list):
    sess = K.get_session()
    K.set_learning_phase(0)
    with sess.as_default():
        model = load_model(input_model_path, compile=False)
        model.trainable = False
        for i in range(len(model.layers)):
            model.layers[i].trainable = False

        # model.compile(optimizer='adam',loss='categorical_crossentropy')
        gd = sess.graph_def
        nodes_with_switch_op = [x for x in gd.node if x.op.lower().find('switch') != -1]
        # nodes = {}
        # for node in gd.node:
        #     nodes[node.name] = node
        # print(len(nodes_with_switch_op))

        # # Rerouting Switch Ops
        for n in [x for x in gd.node]:
            ints = [i for i in n.input]
            endswith1 = [ii for ii in ints if ii.endswith(':1')]  # and 'Switch' in ii]
            if len(endswith1) > 0:
                for index, inn in enumerate(n.input):
                    if inn in endswith1:
                        new_input = inn[:-2]
                        n.input.remove(inn)
                        n.input.insert(index, new_input)
        for n in nodes_with_switch_op:
            n.op = 'Identity'
            n.input.pop()

        # remove_dropout()
        display_nodes(sess.graph.as_graph_def().node)
    # Optimize for inference
    # NOTE: Don't prune graph for image LSTM!
    frozen_graph = graph_util.convert_variables_to_constants(sess, sess.graph.as_graph_def(), final_tensor_list)
    # pruned_graph = graph_util.remove_training_nodes(frozen_graph)
    # tf.reset_default_graph()
    # Convert trained variables to constants

    with gfile.FastGFile(output_model_path, 'wb') as f:
        f.write(frozen_graph.SerializeToString())


# graph = K.get_session().graph
def combine_arch_weights(filename_arch, filename_weights):
    with open(filename_arch + ".json", 'r') as f:
        model = model_from_json(f.read().replace('\n', ''))
    model.load_weights(filename_weights + ".h5")

    model.save(filename_weights + "_combined.h5")


def main():
    sess = K.get_session()
    with sess.as_default():
        # model_img = load_model(CONVLSTM_IMG_GRAPH)
        # model_acc = load_model(CONVLSTM_ACC_GRAPH)
        # model_gyr = load_model(CONVLSTM_GYR_GRAPH)
        # model_mag = load_model(CONVLSTM_MAG_GRAPH)
        graph_def = tf.GraphDef()
        with tf.gfile.FastGFile(CONVLSTM_IMG_PB, 'rb') as fin:
            graph_def.ParseFromString(fin.read())
            _ = tf.import_graph_def(graph_def, name='img')
        with tf.gfile.FastGFile(CONVLSTM_ACC_PB, 'rb') as fin:
            graph_def.ParseFromString(fin.read())
            _ = tf.import_graph_def(graph_def, name='acc')
        with tf.gfile.FastGFile(CONVLSTM_GYR_PB, 'rb') as fin:
            graph_def.ParseFromString(fin.read())
            _ = tf.import_graph_def(graph_def, name='gyr')
        with tf.gfile.FastGFile(CONVLSTM_MAG_PB, 'rb') as fin:
            graph_def.ParseFromString(fin.read())
            _ = tf.import_graph_def(graph_def, name='mag')
        # with tf.gfile.FastGFile(CONVLSTM_FUSION_PB, 'rb') as fin:
        #     graph_def.ParseFromString(fin.read())
        #     _ = tf.import_graph_def(graph_def, name='')
        with tf.gfile.FastGFile(INCEPTION_GRAPH_NAME, 'rb') as fin:
            graph_def.ParseFromString(fin.read())
            _ = tf.import_graph_def(graph_def, name='InceptionGraph')

        display_nodes(sess.graph.as_graph_def().node)

        # with tf.name_scope('StandardizeSensor'):
        #     # Standardization of sensor inputs
        #     acc_normalize_input = tf.placeholder(tf.float32, shape=[75, 3], name='acc_normalize_input')
        #     gyr_normalize_input = tf.placeholder(tf.float32, shape=[75, 3], name='gyr_normalize_input')
        #     mag_normalize_input = tf.placeholder(tf.float32, shape=[75, 3], name='mag_normalize_input')
        #
        #     acc_mean = tf.constant(1, dtype=tf.float32)
        #     gyr_mean = tf.constant(1, dtype=tf.float32)
        #     mag_mean = tf.constant(1, dtype=tf.float32)
        #     acc_std = tf.constant(1, dtype=tf.float32)
        #     gyr_std = tf.constant(1, dtype=tf.float32)
        #     mag_std = tf.constant(1, dtype=tf.float32)
        #
        #     acc_normalize_output = tf.divide(tf.subtract(acc_normalize_input, acc_mean), acc_std,
        #                                      name='acc_normalize_output')
        #     gyr_normalize_output = tf.divide(tf.subtract(gyr_normalize_input, gyr_mean), gyr_std,
        #                                      name='gyr_normalize_output')
        #     mag_normalize_output = tf.divide(tf.subtract(mag_normalize_input, mag_mean), mag_std,
        #                                      name='mag_normalize_output')
        with tf.name_scope('Fusion'):
            img_recog = tf.placeholder(tf.float32, shape=[1, NUM_CLASSES], name='img_recog')
            acc_recog = tf.placeholder(tf.float32, shape=[1, NUM_CLASSES], name='acc_recog')
            gyr_recog = tf.placeholder(tf.float32, shape=[1, NUM_CLASSES], name='gyr_recog')
            mag_recog = tf.placeholder(tf.float32, shape=[1, NUM_CLASSES], name='mag_recog')

            img_biased = tf.scalar_mul(1, img_recog)
            acc_biased = tf.scalar_mul(1, acc_recog)
            gyr_biased = tf.scalar_mul(1, gyr_recog)
            mag_biased = tf.scalar_mul(1, mag_recog)
            recog_concat = tf.concat([img_biased, acc_biased, gyr_biased, mag_biased], axis=0, name="recog_concat")

            # Simple average
            fusion_all_ave = tf.reduce_mean(recog_concat, axis=0, name='fusion_all_ave')

        # with tf.name_scope('Mean'):
        #     recognitions = tf.placeholder(tf.float32, shape=[None, 9], name='mean_input')
        #     recognitions_mean = tf.reduce_mean(recognitions, axis=0, name='mean_output')
        display_nodes(sess.graph.as_graph_def().node)
        # pruned_graph = graph_util.remove_training_nodes(sess.graph.as_graph_def())
        output_names = [IMG_FINAL_TENSOR, ACC_FINAL_TENSOR, GYR_FINAL_TENSOR,
                        MAG_FINAL_TENSOR, INCEPTION_POOL_TENSOR_NAME,
                        FUSION_AVE_OUTPUT_TENSOR,
                        # FUSION_WEIGHTED_AVE_OUTPUT_TENSOR,
                        # FUSION_OUTPUT_TENSOR,
                        # STANDARDIZE_ACC_OUTPUT_TENSOR,
                        # STANDARDIZE_GYR_OUTPUT_TENSOR,
                        # STANDARDIZE_MAG_OUTPUT_TENSOR,
                        # 'Mean/mean_input',
                        # 'Mean/mean_output',
                        ]
        # input_names = [IMG_INPUT_TENSOR, ACC_INPUT_TENSOR, GYR_INPUT_TENSOR, MAG_INPUT_TENSOR, INCEPTION_INPUT_TENSOR]
        # output_graph = optimize_for_inference(sess.graph.as_graph_def(),
        #                                       input_node_names=input_names,
        #                                       output_node_names=output_names,
        #                                       placeholder_type_enum=dtypes.float32.as_datatype_enum)
        K.set_learning_phase(0)
        output_graph = graph_util.convert_variables_to_constants(sess, sess.graph.as_graph_def(),
                                                                 output_names)
        with gfile.FastGFile(UNIFIED_PB_FILENAME, 'wb') as f:
            f.write(output_graph.SerializeToString())


def kpb():
    IMG_TENSOR_KERAS = 'pred_img/Softmax'
    ACC_TENSOR_KERAS = 'pred_acc/Softmax'
    GYR_TENSOR_KERAS = 'pred_gyr/Softmax'
    MAG_TENSOR_KERAS = 'pred_mag/Softmax'
    FUSION_TENSOR_KERAS = 'pred_fusion/Softmax'

    convert_keras_to_tf_pb(CONVLSTM_IMG_GRAPH, CONVLSTM_IMG_PB, [IMG_TENSOR_KERAS])
    convert_keras_to_tf_pb(CONVLSTM_ACC_GRAPH, CONVLSTM_ACC_PB, [ACC_TENSOR_KERAS])
    convert_keras_to_tf_pb(CONVLSTM_GYR_GRAPH, CONVLSTM_GYR_PB, [GYR_TENSOR_KERAS])
    convert_keras_to_tf_pb(CONVLSTM_MAG_GRAPH, CONVLSTM_MAG_PB, [MAG_TENSOR_KERAS])
    # convert_keras_to_tf_pb(CONVLSTM_FUSION_GRAPH, CONVLSTM_FUSION_PB, [
    #     IMG_TENSOR_KERAS, ACC_TENSOR_KERAS, GYR_TENSOR_KERAS, MAG_TENSOR_KERAS,
    #     FUSION_TENSOR_KERAS])


def display_nodes_from_pb(filename):
    sess = tf.Session()
    with sess.as_default():
        graph_def = tf.GraphDef()
        with tf.gfile.FastGFile(filename, 'rb') as fin:
            graph_def.ParseFromString(fin.read())
            _ = tf.import_graph_def(graph_def, name='')
        display_nodes(sess.graph.as_graph_def().node)

def display_nodes_from_h5(filename):
    model = load_model(filename, compile=False)
    sess = K.get_session()
    with sess.as_default():
        display_nodes(sess.graph.as_graph_def().node)
if __name__ == '__main__':
    kpb()
    # tf.reset_default_graph()
    main()
