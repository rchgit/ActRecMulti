import tensorflow as tf
from keras import backend as K
from keras.models import Model
from keras.layers import Input, merge, concatenate
from keras.layers.core import Lambda


def slice_batch(x, n_gpus, part):
    """
    Divide the input batch into [n_gpus] slices, and obtain slice no.
    [part].

    i.e. if len(x)=10, then slice_batch(x, 2, 1) will return x[5:].
    """
    if type(x) != list:
        sh = K.shape(x)
    else:
        sh = K.shape(x[0])
    L = sh[0] // n_gpus
    if part == n_gpus - 1:
        # IF last partition
        if type(x) != list:
            return x[part * L:]
        else:
            return [in_data[part * L:] for in_data in x]
    if type(x) != list:
        return x[part * L:(part + 1) * L]
    else:
        return [in_data[part * L:(part + 1) * L] for in_data in x]


def to_multi_gpu(model, n_gpus=8):
    """Given a keras [model], return an equivalent model which parallelizes
    the computation over [n_gpus] GPUs.

    Each GPU gets a slice of the input batch, applies the model on that
    slice and later the outputs of the models are concatenated to a single
    tensor, hence the user sees a model that behaves the same as the original.
    """
    x = []
    with tf.device('/cpu:0'):
        for input_layer in model.input_layers:
            x.append(Input(input_layer.input_shape[1:], name=input_layer.name))
    # x_concat = concatenate(x,axis=0)
    towers = []

    for g in range(n_gpus):
        with tf.device('/gpu:' + str(g)):
            slice_g = []
            for in_data in x:
                slice_g.append(Lambda(slice_batch, lambda shape: shape,
                                      arguments={'n_gpus': n_gpus, 'part': g})(in_data))
            towers.append(model(slice_g))
            print('/gpu:' + str(g))
    # merged = []
    with tf.device('/cpu:0'):
        merged = concatenate(towers, axis=0)
        # merged = merge(towers, mode='concat', concat_axis=0)

    return Model(inputs=x, outputs=merged)
