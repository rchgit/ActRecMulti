# %%capture alloutput
# %matplotlib inline
import time

from keras.models import load_model
import tensorflow as tf
from keras.backend import set_session
from trainconfig import cross_validate, TrainConfig, train_3split
from multi_gpu_keras import to_multi_gpu
import os
if __name__ == '__main__':
    sensor_train = 'img'
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    timestamp = time.time()
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    # Hyperparameter optimization
    trials_filename = 'results_img/expts/img_hyperopt_1512140285.0873957.expt'

    # Cross-validation
    k_folds = 5
    num_epochs = 100
    # NOTE: this train_config is different from the one used internally in run_hyperopt!

    train_config = TrainConfig()
    train_config.SENSOR_TRAIN = sensor_train
    train_config.DATA_PATH = "dataset_mead"
    train_config.CHECKPOINTS_DIR = "results_" + train_config.SENSOR_TRAIN + "/chkpts/"
    train_config.EXPT_FILENAME = trials_filename
    train_config.LABELS_FILE = 'har_labels_mead.txt'
    dict_result, model_path = train_3split(train_config, num_epochs=num_epochs, config=config)
    for key, value in dict_result.items():
        print(key + ' : ' + str(value))

    input("Press Enter to continue...")
