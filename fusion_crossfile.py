import time

import tensorflow as tf

from trainconfig import TrainConfig, train_3split, hyperspace_fusion_file

if __name__ == '__main__':
    sensor_train = 'fusion_file'
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    timestamp = time.time()

    # Hyperparameter optimization
    # trials_filename = 'results_fusion_full/expts/fusion_full_hyperopt_1508922600.7540615-codetest.expt'
    # NOTE: For logistic regression, the parameters don't matter... They're ignored anyway
    trials_filename = 'results_fusion_file/expts/fusion_file_hyperopt_1509539753.1830173.expt'

    # Cross-validation
    k_folds = 5
    num_epochs = 100
    # NOTE: this train_config is different from the one used internally in run_hyperopt!
    train_config = TrainConfig()
    train_config.SENSOR_TRAIN = sensor_train
    train_config.DATA_PATH = "dataset_aded"
    train_config.LABELS_FILE = 'har_labels_aded.txt'
    train_config.CHECKPOINTS_DIR = "results_" + train_config.SENSOR_TRAIN + "/chkpts/"
    train_config.EXPT_FILENAME = trials_filename
    # train_config.IMG_CHKPT_FILENAME = "results_img/chkpts/best_img_1510487176_aded_weights_combined.h5"
    # train_config.ACC_CHKPT_FILENAME = "results_acc/chkpts/best_acc_1509110217_aded_weights_combined.h5"
    train_config.IMG_CHKPT_FILENAME = "results_img/chkpts/best_img_1510487176_aded_weights_combined.h5-over.h5"
    train_config.ACC_CHKPT_FILENAME = "results_acc/chkpts/best_acc_1509110217_aded_weights_combined.h5-over.h5"
    train_config.GYR_CHKPT_FILENAME = "results_gyr/chkpts/best_gyr_1509114072_aded_weights_combined.h5"
    train_config.MAG_CHKPT_FILENAME = "results_mag/chkpts/best_mag_1509108912_aded_weights_combined.h5"
    dict_result, best_model_path = train_3split(train_config,
                                                num_epochs=num_epochs,
                                                dataset=train_config.DATA_PATH, hyper_space=hyperspace_fusion_file,
                                                train_on_scores=True)

    # for key, value in dict_result.items():
    #     print(key + ' : ' + value)

    # train_config.DATA_PATH = "dataset_mead"
    # dataset = train_config.DATA_PATH
    # train_config.CHECKPOINTS_DIR = "results_mead_" + train_config.SENSOR_TRAIN + "/chkpts/"
    # dict_result_mead, model_mead_path = train_3split(train_config,
    #                                                  num_epochs=num_epochs,
    #                                                  dataset=dataset, hyper_space=hyperspace_fusion_file,
    #                                                  )
    # # dict_result_mead, model_mead_path = train_3split(train_config, timestamp=timestamp,
    # #                                                  config=config)
    # print(dict_result_mead)
    # print("Saved MEAD retrained model to: " + model_mead_path)
