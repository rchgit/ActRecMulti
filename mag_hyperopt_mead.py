# %%capture alloutput
# %matplotlib inline
from trainconfig import expt_runner
import tensorflow as tf
import os

if __name__ == '__main__':
    os.environ["CUDA_VISIBLE_DEVICES"] = "4"
    sensor_train = 'mag'
    expt_runner(sensor_train, num_trials=10, num_epochs_hyperopt=10, k_folds=5, num_epochs_cross=100,
                dataset='dataset_mead', gpu=None)
