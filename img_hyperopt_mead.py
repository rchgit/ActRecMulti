# %%capture alloutput
# %matplotlib inline
from trainconfig import expt_runner
import tensorflow as tf
import os

if __name__ == '__main__':
    os.environ["CUDA_VISIBLE_DEVICES"] = "1"
    sensor_train = 'img'

    expt_runner(sensor_train, num_trials=10, num_epochs_hyperopt=10, k_folds=5, num_epochs_cross=50,
                dataset='dataset_mead', gpu=None)
