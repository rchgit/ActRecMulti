import os
import pickle
import time

import numpy as np
import tensorflow as tf
from hyperopt import hp, space_eval
from keras import backend as K, Input, metrics, callbacks
from keras.engine import Model
from keras.layers import Dense, Dropout, Concatenate
from keras.models import load_model
from keras.utils import to_categorical
from sklearn.metrics import confusion_matrix
from trainconfig import TrainConfig, get_labels, mkdir_conditional

if __name__ == '__main__':
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    K.set_session(sess)
    timestamp = time.time()

    seq_length = {
        'vid': 30,
        'acc': 120,
        'gyr': 120,
        'mag': 60,
    }
    seq_stride = 5
    feat_vec_length = {
        'vid': 1024,
        'acc': 3,
        'gyr': 3,
        'mag': 3,
    }

    # Cross-validation
    k_folds = 5
    num_epochs = 100
    NUM_CLASSES = 9
    # NOTE: this train_config is different from the one used internally in run_hyperopt!
    train_config = TrainConfig()

    train_config.DATA_PATH = "dataset_aded"
    train_config.LABELS_FILE = 'har_labels_aded.txt'

    # train_config.IMG_CHKPT_FILENAME = "results_img/chkpts/best_img_1510487176_aded_weights_com    bined.h5"
    # train_config.ACC_CHKPT_FILENAME = "results_acc/chkpts/best_acc_1509110217_aded_weights_combined.h5"
    train_config.IMG_CHKPT_FILENAME = "results_img/chkpts/best_img_1510487176_aded_weights_combined.h5-over.h5"
    train_config.ACC_CHKPT_FILENAME = "results_acc/chkpts/best_acc_1509110217_aded_weights_combined.h5-over.h5"
    train_config.GYR_CHKPT_FILENAME = "results_gyr/chkpts/best_gyr_1509114072_aded_weights_combined.h5"
    train_config.MAG_CHKPT_FILENAME = "results_mag/chkpts/best_mag_1509108912_aded_weights_combined.h5"

    ############# EXPERIMENTS HERE #########
    model = load_model(train_config.GYR_CHKPT_FILENAME)
    # Load csv data file
    filename = "../actual_data/files/data/data-1517815523266-gyr.csv"
    assert filename.endswith('.csv')
    # lstm_sensor_feats = []
    signal_array = np.genfromtxt(
        filename,
        dtype=np.float32,
        delimiter=',')
    signal_array = signal_array[:, 1:4]
    # if scale:
    #     signal_array = (signal_array - mins[sensor]) / ranges[sensor]
    sensor = 'acc'
    for i in range(0, np.size(signal_array, axis=0), seq_stride):
        # Add to the queue.
        # In order to turn our discrete predictions or features into a sequence,
        # we loop through each frame in chronological order, add it to a queue of size N,
        # and pop off the first frame we previously added.
        # N represents the length of our sequence that we'll pass to the RNN.
        sequence = signal_array[i:i + seq_length[sensor], :]
        # Zero-padding if sequence is shorter than prescribed length
        if sequence.shape[0] < seq_length[sensor]:
            sequence = np.append(sequence,
                                 np.zeros(
                                     [seq_length[sensor] - sequence.shape[0],
                                      feat_vec_length[sensor]]),
                                 axis=0)
        # Perform inference
        lstm_feat = model.predict(np.expand_dims(sequence, 0), batch_size=1)

        print(lstm_feat)
        print(np.argmax(lstm_feat, axis=1))
