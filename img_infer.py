import time
import os
import pickle
from functools import partial

import numpy as np
import imageio
from keras.engine import Model
from keras.layers import Dense
from keras.models import load_model
from keras import metrics
import tensorflow as tf
from keras import backend as K, callbacks
from keras.backend import set_session
from keras.utils import to_categorical

from convert_keras_to_tf import display_nodes
from trainconfig import cross_validate, TrainConfig, train_3split, mkdir_conditional
from multi_gpu_keras import to_multi_gpu

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
K.set_session(sess)

if __name__ == "__main__":
    CONVLSTM_IMG_GRAPH = "results_img/chkpts/best_img_1510487176_aded_weights_combined.h5-over-new.h5"
    filename = "dataset_over/acc/upstairs/1515662526398-acc.csv"
    test_data = np.genfromtxt(filename, dtype=np.float32, delimiter=',',
                              missing_values='', filling_values=0, invalid_raise=False)
    test_data = np.expand_dims(test_data, axis=0)
    test_data = test_data[:, 23:53, :1024]
    test_data = 0.9 * np.ones(shape=(1, 30, 1024), dtype=np.float32)
    top3 = partial(metrics.top_k_categorical_accuracy, k=3)
    top3.__name__ = 'top3_categorical_accuracy'
    model = load_model(CONVLSTM_IMG_GRAPH, custom_objects={'top3_categorical_accuracy': top3})
    model.trainable = False
    m = Model(inputs=[model.layers[1].input], outputs=[model.output])
    m.compile(optimizer='adam', loss='categorical_crossentropy')
    print(m.predict_on_batch(test_data))
