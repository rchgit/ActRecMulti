import time
import os
import pickle
import numpy as np
import imageio
from keras.engine import Model
from keras.layers import Dense
from keras.models import load_model
from keras import metrics
import tensorflow as tf
from keras import backend as K, callbacks
from keras.backend import set_session
from keras.utils import to_categorical
from sklearn import preprocessing

from convert_keras_to_tf import display_nodes
from trainconfig import cross_validate, TrainConfig, train_3split, mkdir_conditional
from multi_gpu_keras import to_multi_gpu

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
K.set_session(sess)

try:
    with open('error_videos.txt', 'r') as file:
        error_video_filenames = file.read().split("\n")
except IOError:
    error_video_filenames = []
    print("No error video file list yet, creating one.")


def get_labels(labels_filename=None, data_path=None, sensor_train=None):
    if labels_filename is None and data_path is not None and sensor_train is not None:
        LABELS = ([x for x in os.listdir(data_path + "/" + sensor_train)
                   if os.path.isdir(data_path + "/" + sensor_train + "/" + x)])
        with open('har_labels.txt', 'w') as labels_file:
            labels_file.write("\n".join(LABELS))
    else:
        with open(labels_filename, 'r') as labels_file:
            LABELS = [line.strip('\n') for line in labels_file]
    return LABELS


def data(train_config):
    LABELS = get_labels(train_config.LABELS_FILE, train_config.DATA_PATH, train_config.SENSOR_TRAIN)
    for label in LABELS:
        mkdir_conditional(train_config.FEAT_CACHE_PATH + "/" + label)



    # Data acquisition
    lstm_features = []
    lstm_classes = []
    for class_index, label in enumerate(LABELS):
        # print('Class index: ' + str(class_index))
        for root, dirs, files in os.walk(train_config.DATA_PATH + "/" + label + "/" + train_config.SENSOR_TRAIN):
            for filename in files:
                # Read csv into numpy array
                if filename.endswith('.csv'):
                    try:
                        signal_array = np.genfromtxt(
                            train_config.DATA_PATH + "/" + label + "/" + train_config.SENSOR_TRAIN + "/" + filename,
                            dtype=np.float32,
                            delimiter=',')
                        signal_array = signal_array[:, 1:4]
                        signal_array = (signal_array - train_config.MIN) / train_config.RANGE

                        for i in range(0, np.size(signal_array, axis=0), train_config.SEQ_STRIDE):
                            # Add to the queue.
                            # In order to turn our discrete predictions or features into a sequence,
                            # we loop through each frame in chronological order, add it to a queue of size N,
                            # and pop off the first frame we previously added.
                            # N represents the length of our sequence that we'll pass to the RNN.
                            sequence = signal_array[i:i + train_config.SEQ_LENGTH, :]
                            # Zero-padding if sequence is shorter than prescribed length
                            if sequence.shape[0] < train_config.SEQ_LENGTH:
                                sequence = np.append(sequence,
                                                     np.zeros(
                                                         [train_config.SEQ_LENGTH - sequence.shape[0],
                                                          train_config.FEAT_VEC_LENGTH]),
                                                     axis=0)
                            lstm_features.append(sequence)
                            lstm_classes.append(class_index)

                    except Exception as e:
                        print(e)
                        continue

    # Numpy.
    lstm_features = np.rollaxis(np.dstack(lstm_features), axis=-1)

    # Split into train and validation.

    lstm_features = np.rollaxis(np.dstack(lstm_features), axis=-1)
    lstm_classes = to_categorical(np.array(lstm_classes), len(LABELS))

    return lstm_features, lstm_classes, LABELS


if __name__ == "__main__":
    # acc
    # Min: [-19.54488373 - 19.65252686 - 20.2605381]
    # acc
    # Range: [39.44257355  39.35431671  39.86426163]
    # CONVLSTM_IMG_GRAPH = "results_mag/chkpts/best_mag_1511858143_aded_combined.h5"
    CONVLSTM_IMG_GRAPH = "results_mag/chkpts/best_mag_1512053475_aded_combined.h5"

    train_config = TrainConfig()
    train_config.SENSOR_TRAIN = 'mag'
    train_config.FEAT_CACHE_PATH = "feats_for_new_algo"
    train_config.DATA_PATH = "data_for_new_algo"
    train_config.NUM_EPOCHS = 10
    train_config.BATCH_SIZE = 8
    train_config.CNN_INPUT_TENSOR_NAME = "Cast:0"
    train_config.SEQ_LENGTH = 60
    train_config.SEQ_STRIDE = 5
    train_config.MIN = [-76.40000153, -54.5, -75.]
    train_config.RANGE = [157.30000305, 107.5, 132.]
    train_config.LABELS_FILE = "har_labels_data_for_new_algo.txt"
    X_train, y_train, LABELS = data(train_config)

    model_old = load_model(CONVLSTM_IMG_GRAPH)
    model_old.trainable = False
    old_output = model_old.layers[-2].output
    new_output = Dense(len(LABELS), activation='softmax', name='pred_' + train_config.SENSOR_TRAIN)(old_output)
    model = Model(inputs=model_old.inputs, outputs=new_output)
    model.compile(optimizer='adam', loss='categorical_crossentropy',
                  metrics=['accuracy', 'categorical_accuracy', 'top_k_categorical_accuracy', metrics.mae,
                           metrics.mse])
    earlystop = callbacks.EarlyStopping(monitor='val_acc', min_delta=0, patience=0, verbose=1, mode='auto')
    time_start = time.time()
    model_history = model.fit(X_train, y_train,
                              batch_size=train_config.BATCH_SIZE,
                              epochs=train_config.NUM_EPOCHS,
                              shuffle=False,
                              callbacks=[earlystop],
                              validation_split=0.1
                              )

    model.save(CONVLSTM_IMG_GRAPH + "-over-new.h5")
    K.clear_session()
