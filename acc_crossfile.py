# %%capture alloutput
# %matplotlib inline
import time

import os
import tensorflow as tf

from trainconfig import TrainConfig, train_3split

if __name__ == '__main__':
    # os.environ["CUDA_VISIBLE_DEVICES"] = "2"
    sensor_train = 'acc'
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    timestamp = time.time()

    # Hyperparameter optimization
    trials_filename = 'results_acc/expts/acc_hyperopt_1511779502.3713768.expt'

    # Cross-validation
    k_folds = 5
    num_epochs = 100
    # NOTE: this train_config is different from the one used internally in run_hyperopt!
    train_config = TrainConfig()
    train_config.SENSOR_TRAIN = sensor_train
    train_config.DATA_PATH = "dataset_aded"
    train_config.CHECKPOINTS_DIR = "results_" + train_config.SENSOR_TRAIN + "/chkpts/"
    train_config.EXPT_FILENAME = trials_filename
    train_config.LABELS_FILE = 'har_labels_aded.txt'
    dict_result, model_path = train_3split(train_config, num_epochs=num_epochs, config=config)
    for key, value in dict_result.items():
        print(key + ' : ' + str(value))

    input("Press enter to continue")
