import pickle
import numpy as np
import matplotlib

matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt

acc_sample_aded = {}
acc_sample_aded['downstairs'] = np.genfromtxt('dataset_aded/acc/downstairs/1498472035706-acc.csv', dtype=np.float32,
                                              delimiter=',')
acc_sample_aded['eating'] = np.genfromtxt('dataset_aded/acc/eating/1498526885447-acc.csv', dtype=np.float32,
                                          delimiter=',')
acc_sample_aded['jumping'] = np.genfromtxt('dataset_aded/acc/jumping/1498379851597-acc.csv', dtype=np.float32,
                                           delimiter=',')
acc_sample_aded['sitting'] = np.genfromtxt('dataset_aded/acc/sitting/1500016798626-acc.csv', dtype=np.float32,
                                           delimiter=',')
acc_sample_aded['standing'] = np.genfromtxt('dataset_aded/acc/standing/1500727921957-acc.csv', dtype=np.float32,
                                            delimiter=',')
acc_sample_aded['still'] = np.genfromtxt('dataset_aded/acc/still/1498368311696-acc.csv', dtype=np.float32,
                                         delimiter=',')
acc_sample_aded['upstairs'] = np.genfromtxt('dataset_aded/acc/upstairs/1500023091548-acc.csv', dtype=np.float32,
                                            delimiter=',')
acc_sample_aded['walking'] = np.genfromtxt('dataset_aded/acc/walking/1498383721072-acc.csv', dtype=np.float32,
                                           delimiter=',')

for k in acc_sample_aded.keys():
    acc_sample_aded[k] = acc_sample_aded[k][:, 1:4]

with open('acc_sample.pkl', 'wb') as f:
    pickle.dump(acc_sample_aded, f)
