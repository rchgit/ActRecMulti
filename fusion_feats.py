import os
import pickle
import time

import numpy as np
import tensorflow as tf
from hyperopt import hp, space_eval
from keras import backend as K, Input, metrics, callbacks
from keras.engine import Model
from keras.layers import Dense, Dropout, Concatenate
from keras.models import load_model
from keras.utils import to_categorical
from sklearn.metrics import confusion_matrix
from trainconfig import TrainConfig, get_labels, mkdir_conditional


def inference(train_config):
    seq_length = {
        'vid': 30,
        'acc': 120,
        'gyr': 120,
        'mag': 60,
    }
    seq_stride = 5
    feat_vec_length = {
        'vid': 1024,
        'acc': 3,
        'gyr': 3,
        'mag': 3,
    }

    LABELS = get_labels(train_config.LABELS_FILE, train_config.DATA_PATH, 'img')

    print(LABELS)

    vid_cache_path = "features_vid_lstm"
    is_vid_feat_csv = False
    sensor_cache = {
        'acc': "features_acc_lstm",
        'gyr': "features_gyr_lstm",
        'mag': "features_mag_lstm",
    }
    mins = {
        'acc': [-19.54488373, -19.65252686, -20.2605381],
        'gyr': [-17.44252586, -11.30822563, -11.161129],
        'mag': [-76.40000153, -54.5, -75.]
    }
    ranges = {
        'acc': [39.44257355, 39.35431671, 39.86426163],
        'gyr': [28.76673698, 24.50736618, 22.47148132],
        'mag': [157.30000305, 107.5, 132.]
    }

    for label in LABELS:
        mkdir_conditional(vid_cache_path + "/" + label)
        for key in sensor_cache.keys():
            mkdir_conditional(sensor_cache[key] + "/" + label)

    # Data acquisition
    lstm_classes = {
        'vid': [],
        'acc': [],
        'gyr': [],
        'mag': [],
    }
    lstm_feature_list = {
        'vid': [],
        'acc': [],
        'gyr': [],
        'mag': [],
    }
    sensor_model = {
        'acc': None,
        'gyr': None,
        'mag': None,
    }
    vid_model = load_model(train_config.IMG_CHKPT_FILENAME)
    sensor_model['acc'] = load_model(train_config.ACC_CHKPT_FILENAME)
    sensor_model['gyr'] = load_model(train_config.GYR_CHKPT_FILENAME)
    sensor_model['mag'] = load_model(train_config.MAG_CHKPT_FILENAME)

    vid_model.trainable = False
    sensor_model['acc'].trainable = False
    sensor_model['gyr'].trainable = False
    sensor_model['mag'].trainable = False

    scale = True
    for class_index, label in enumerate(LABELS):

        for _, _, files in os.walk(train_config.DATA_PATH + "/img/" + label):
            for filename in files:
                # Read lstm feature cache
                if is_vid_feat_csv:
                    lstm_vid_feat_filename = vid_cache_path + "/" + label + "/" + filename + ".csv"
                else:
                    lstm_vid_feat_filename = vid_cache_path + "/" + label + "/" + filename + ".vidlstmfeat"
                try:
                    with open(lstm_vid_feat_filename, 'rb') as lstm_feat_file:
                        lstm_vid_feats = pickle.load(lstm_feat_file)
                except FileNotFoundError:
                    try:
                        lstm_vid_feats = []
                        full_filename = "features_img/" + label + "/" + filename + ".feature"
                        # For each Inception feature
                        if os.path.isfile(full_filename):
                            with open(full_filename, 'rb') as cnn_feat_file:
                                cnn_feats = pickle.load(cnn_feat_file)
                        for i in range(0, cnn_feats.shape[0], seq_stride):
                            sequence = cnn_feats[i:i + seq_length['vid'], :]
                            if sequence.shape[0] < seq_length['vid']:
                                sequence = np.append(sequence,
                                                     np.zeros(
                                                         [seq_length['vid'] - sequence.shape[0],
                                                          feat_vec_length['vid']]),
                                                     axis=0)

                            lstm_feat = vid_model.predict(np.expand_dims(sequence, 0), batch_size=1)
                            if np.size(sequence, 0) == seq_length['vid']:
                                lstm_vid_feats.append(lstm_feat)
                        assert len(lstm_vid_feats) > 0
                        with open(lstm_vid_feat_filename, 'wb') as f:
                            pickle.dump(lstm_vid_feats, f)
                    except AssertionError:
                        continue
                    except Exception as e:
                        print(str(e))
                        continue
                lstm_feature_list['vid'].append(lstm_vid_feats)
                lstm_classes['vid'].append(np.full(len(lstm_vid_feats), class_index))
        for sensor in ['acc', 'gyr', 'mag']:
            for _, _, files in os.walk(train_config.DATA_PATH + "/" + sensor + "/" + label):
                for filename in files:
                    lstm_sensor_feat_filename = sensor_cache[
                                                    sensor] + "/" + label + "/" + filename + "." + sensor + "lstmfeat"
                    try:
                        with open(lstm_sensor_feat_filename, 'rb') as lstm_feat_file:
                            lstm_sensor_feats = pickle.load(lstm_feat_file)
                    except FileNotFoundError:
                        # Read csv into numpy array
                        try:
                            assert filename.endswith('.csv')
                            lstm_sensor_feats = []
                            signal_array = np.genfromtxt(
                                train_config.DATA_PATH + "/" + sensor + "/" + label + "/" + filename,
                                dtype=np.float32,
                                delimiter=',')
                            signal_array = signal_array[:, 1:4]
                            if scale:
                                signal_array = (signal_array - mins[sensor]) / ranges[sensor]

                            for i in range(0, np.size(signal_array, axis=0), seq_stride):
                                # Add to the queue.
                                # In order to turn our discrete predictions or features into a sequence,
                                # we loop through each frame in chronological order, add it to a queue of size N,
                                # and pop off the first frame we previously added.
                                # N represents the length of our sequence that we'll pass to the RNN.
                                sequence = signal_array[i:i + seq_length[sensor], :]
                                # Zero-padding if sequence is shorter than prescribed length
                                if sequence.shape[0] < seq_length[sensor]:
                                    sequence = np.append(sequence,
                                                         np.zeros(
                                                             [seq_length[sensor] - sequence.shape[0],
                                                              feat_vec_length[sensor]]),
                                                         axis=0)
                                # Perform inference
                                lstm_feat = sensor_model[sensor].predict(np.expand_dims(sequence, 0), batch_size=1)

                                if np.size(sequence, 0) == seq_length[sensor]:
                                    lstm_sensor_feats.append(lstm_feat)
                                assert len(lstm_sensor_feats) > 0
                                with open(lstm_sensor_feat_filename, 'wb') as f:
                                    pickle.dump(lstm_sensor_feats, f)
                        except AssertionError as e:
                            print(e)
                            continue
                    except Exception as e:
                        print(str(e))
                        continue

                    lstm_feature_list[sensor].append(lstm_sensor_feats)
                    lstm_classes[sensor].append(class_index)
    return lstm_feature_list, lstm_classes


if __name__ == '__main__':
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    K.set_session(sess)
    timestamp = time.time()

    # Hyperparameter optimization
    # trials_filename = 'results_fusion_full/expts/fusion_full_hyperopt_1508922600.7540615-codetest.expt'
    # NOTE: For logistic regression, the parameters don't matter... They're ignored anyway
    hyperspace_fusion_file = {
        'dense_1': hp.choice('dense_1', [
            False,
            {
                'dense_1_num_units': hp.quniform('dense_1_num_units', 1, 20, 1),
            }
        ]),

        'dropout_prob': hp.loguniform('dropout_prob', np.log(0.1), np.log(0.5))
    }
    trials_filename = 'results_fusion_file/expts/fusion_file_hyperopt_1509539753.1830173.expt'
    with open(trials_filename, 'rb') as f:
        trials = pickle.load(f)
        best_trial = trials.argmin
        hyperspace = space_eval(hyperspace_fusion_file, best_trial)
    # Cross-validation
    k_folds = 5
    num_epochs = 100
    NUM_CLASSES = 9
    # NOTE: this train_config is different from the one used internally in run_hyperopt!
    train_config = TrainConfig()

    train_config.DATA_PATH = "dataset_aded"
    train_config.LABELS_FILE = 'har_labels_aded.txt'
    train_config.EXPT_FILENAME = trials_filename
    # train_config.IMG_CHKPT_FILENAME = "results_img/chkpts/best_img_1510487176_aded_weights_combined.h5"
    # train_config.ACC_CHKPT_FILENAME = "results_acc/chkpts/best_acc_1509110217_aded_weights_combined.h5"
    train_config.IMG_CHKPT_FILENAME = "results_img/chkpts/best_img_1510487176_aded_weights_combined.h5-over.h5"
    train_config.ACC_CHKPT_FILENAME = "results_acc/chkpts/best_acc_1509110217_aded_weights_combined.h5-over.h5"
    train_config.GYR_CHKPT_FILENAME = "results_gyr/chkpts/best_gyr_1509114072_aded_weights_combined.h5"
    train_config.MAG_CHKPT_FILENAME = "results_mag/chkpts/best_mag_1509108912_aded_weights_combined.h5"
    lstm_features, lstm_classes = inference(train_config)

    K.clear_session()

    input_img = Input(shape=(NUM_CLASSES,))
    input_acc = Input(shape=(NUM_CLASSES,))
    input_gyr = Input(shape=(NUM_CLASSES,))
    input_mag = Input(shape=(NUM_CLASSES,))

    concat = Concatenate()([input_img, input_acc, input_gyr, input_mag])
    if hyperspace['dense_1']:
        concat = Dense(int(hyperspace['dense_1']['dense_1_num_units']), activation='relu',
                       # kernel_regularizer=regularizers.l2(hyperspace['dense_1']['dense_1_l2_strength'])
                       )(concat)
    concat = Dropout(hyperspace['dropout_prob'])(concat)
    pred_fusion = Dense(NUM_CLASSES, activation='softmax', name='pred_fusion',
                        # kernel_regularizer=regularizers.l2(hyperspace['dense_last']['dense_last_l2_strength'])
                        )(concat)

    model_fusion = Model(inputs=[input_img, input_acc, input_gyr, input_mag],
                         outputs=[pred_fusion])
    model_fusion.compile(optimizer='adam', loss='categorical_crossentropy',
                         metrics=['accuracy', 'categorical_accuracy', 'top_k_categorical_accuracy', metrics.mae,
                                  metrics.mse])
    earlystop = callbacks.EarlyStopping(monitor='val_loss', min_delta=0.0000000001, patience=10, verbose=1, mode='auto')
    classes = []
    vid = [np.repeat(f, repeats=4, axis=0) for f in lstm_features['vid']]
    mag = [np.repeat(f, repeats=2, axis=0) for f in lstm_features['mag']]
    acc = [np.array(f) for f in lstm_features['acc']]
    gyr = [np.array(f) for f in lstm_features['gyr']]
    for i in range(len(vid)):
        min_len = np.min(np.array([vid[i].shape[0], acc[i].shape[0], gyr[i].shape[0], mag[i].shape[0]]))
        vid[i] = vid[i][0:min_len]
        acc[i] = acc[i][0:min_len]
        gyr[i] = gyr[i][0:min_len]
        mag[i] = mag[i][0:min_len]
        classes.append(np.repeat(lstm_classes['acc'][i], min_len))
    vid = np.squeeze(np.concatenate(vid), axis=1)
    acc = np.squeeze(np.concatenate(acc), axis=1)
    gyr = np.squeeze(np.concatenate(gyr), axis=1)
    mag = np.squeeze(np.concatenate(mag), axis=1)
    classes = to_categorical(np.concatenate(classes), num_classes=NUM_CLASSES)
    model_history = model_fusion.fit(
        [vid, acc, gyr, mag],
        [classes],
        # validation_data=(
        #     [X_valid['img'], X_valid['acc'], X_valid['gyr'], X_valid['mag']],
        #     [y_valid]),
        batch_size=64,
        epochs=num_epochs,
        shuffle=False,
        # callbacks=[earlystop]
        )
    model_fusion.save('fusion_retrained.h5')
    # metrics_list = model_fusion.evaluate(
    #     [lstm_features['img'], lstm_features['acc'], lstm_features['gyr'], lstm_features['mag']], y_test,
    #     batch_size=train_config.BATCH_SIZE)
    # y_pred = model_fusion.predict(
    #     [lstm_features['img'], lstm_features['acc'], lstm_features['gyr'], lstm_features['mag']], verbose=1)
    # y_pred_train = model_fusion.predict([X_train['img'], X_train['acc'], X_train['gyr'], X_train['mag']], verbose=1)
    # y_pred_valid = model_fusion.predict([X_valid['img'], X_valid['acc'], X_valid['gyr'], X_valid['mag']], verbose=1)
    # print('Current timestamp: ' + str(timestamp))
    # print("Confusion matrix: ")
    # confusion = confusion_matrix(np.argmax(lstm_classes, axis=1), np.argmax(y_pred, axis=1))
    # confusion_train = confusion_matrix(np.argmax(y_train, axis=1), np.argmax(y_pred_train, axis=1))
    # confusion_valid = confusion_matrix(np.argmax(y_valid, axis=1), np.argmax(y_pred_valid, axis=1))
    # print('Current timestamp: ' + str(timestamp))
    # print("Confusion matrix: ")
