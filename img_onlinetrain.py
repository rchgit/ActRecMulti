import time
import os
import pickle
from functools import partial

import numpy as np
import imageio
from keras.engine import Model
from keras.layers import Dense
from keras.models import load_model
from keras import metrics
import tensorflow as tf
from keras import backend as K, callbacks
from keras.backend import set_session
from keras.utils import to_categorical

from convert_keras_to_tf import display_nodes
from trainconfig import cross_validate, TrainConfig, train_3split, mkdir_conditional
from multi_gpu_keras import to_multi_gpu

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
K.set_session(sess)

try:
    with open('error_videos.txt', 'r') as file:
        error_video_filenames = file.read().split("\n")
except IOError:
    error_video_filenames = []
    print("No error video file list yet, creating one.")


def get_labels(labels_filename=None, data_path=None, sensor_train=None):
    if labels_filename is None and data_path is not None and sensor_train is not None:
        LABELS = ([x for x in os.listdir(data_path + "/" + sensor_train)
                   if os.path.isdir(data_path + "/" + sensor_train + "/" + x)])
        with open('har_labels.txt', 'w') as labels_file:
            labels_file.write("\n".join(LABELS))
    else:
        with open(labels_filename, 'r') as labels_file:
            LABELS = [line.strip('\n') for line in labels_file]
    return LABELS


def data(train_config):
    LABELS = get_labels(train_config.LABELS_FILE, train_config.DATA_PATH, train_config.SENSOR_TRAIN)
    for label in LABELS:
        mkdir_conditional(train_config.FEAT_CACHE_PATH + "/" + label)

    # Data acquisition
    lstm_features = []
    lstm_classes = []
    sensor_train = 'img'

    with tf.gfile.FastGFile(train_config.FEAT_EXTRACTOR_NAME, 'rb') as fin:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(fin.read())
        _ = tf.import_graph_def(graph_def, name='')
    display_nodes(sess.graph.as_graph_def().node)
    for class_index, label in enumerate(LABELS):
        for root, dirs, files in os.walk(train_config.DATA_PATH + "/" + label + "/" + train_config.SENSOR_TRAIN):
            for filename in files:
                lstm_feature_list = []
                # Read bottleneck cache
                cnn_feat_filename = train_config.FEAT_CACHE_PATH + "/" + label + "/" + filename + ".feature"
                if os.path.isfile(cnn_feat_filename):
                    with open(cnn_feat_filename, 'rb') as cnn_feat_file:
                        cnn_feats = pickle.load(cnn_feat_file)

                else:
                    try:
                        full_filename = train_config.DATA_PATH + "/" + label + "/" + train_config.SENSOR_TRAIN + "/" + filename
                        # For each frame
                        with imageio.get_reader(full_filename, format=train_config.VIDEO_READER) as reader:
                            print("Acquiring: " + full_filename)
                            cnn_feats = []

                            try:
                                for frame_index, frame in enumerate(reader):
                                    # if not (frame_index % int(reader._meta['fps'])):
                                    # NOTE: should I add decimation here (e.g. every 10 frames?)
                                    # Run Inception inference
                                    pool_tensor = sess.graph.get_tensor_by_name(train_config.POOL_TENSOR_NAME)
                                    # Add to queue
                                    cnn_feat = sess.run(pool_tensor, {train_config.CNN_INPUT_TENSOR_NAME:
                                                                          frame})
                                    cnn_feats.append(np.squeeze(cnn_feat))
                            except imageio.core.format.CannotReadFrameError:
                                print("Erroneous last frame.")
                                bare_filename, _ = os.path.splitext(filename)
                                error_video_filenames.append(bare_filename)

                            cnn_feats = np.array(cnn_feats)
                            try:
                                with open(cnn_feat_filename, 'wb') as cnn_feat_file:
                                    pickle.dump(cnn_feats, cnn_feat_file)
                                    print("Saving " + cnn_feat_filename)
                            except IOError:
                                print("IOError occurred while saving cache")

                    except Exception as e:
                        print(str(type(e)) + ': ' + str(e))
                        continue

                for i in range(0, cnn_feats.shape[0], train_config.SEQ_STRIDE):
                    sequence = cnn_feats[i:i + train_config.SEQ_LENGTH, :]
                    if np.size(sequence, 0) == train_config.SEQ_LENGTH:
                        lstm_feature_list.append(sequence)
                try:
                    assert len(lstm_feature_list) > 0
                except AssertionError:
                    # print("len(lstm_feature_list) <= 0")
                    continue
                lstm_features.extend(lstm_feature_list)
                lstm_classes.append(np.full(len(lstm_feature_list), class_index))

    lstm_features = np.rollaxis(np.dstack(lstm_features), axis=-1)
    lstm_classes = to_categorical(np.array([item for sublist in lstm_classes for item in sublist]), len(LABELS))

    return lstm_features, lstm_classes, LABELS


if __name__ == "__main__":
    CONVLSTM_IMG_GRAPH = "results_img/chkpts/best_img_1510487176_aded_weights_combined.h5"

    train_config = TrainConfig()
    train_config.FEAT_CACHE_PATH = "feats_for_new_algo"
    train_config.DATA_PATH = "data_for_new_algo"
    train_config.NUM_EPOCHS = 10
    train_config.BATCH_SIZE = 8
    train_config.CNN_INPUT_TENSOR_NAME = "Cast:0"
    train_config.SEQ_LENGTH = 30
    train_config.SEQ_STRIDE = 5
    train_config.LABELS_FILE = "har_labels_data_for_new_algo.txt"
    train_config.SENSOR_TRAIN = "img"
    X_train, y_train, LABELS = data(train_config)

    model_old = load_model(CONVLSTM_IMG_GRAPH)
    model_old.trainable = False
    old_output = model_old.layers[-2].output
    new_output = Dense(len(LABELS), activation='softmax', name='pred_' + train_config.SENSOR_TRAIN)(old_output)
    top3 = partial(metrics.top_k_categorical_accuracy, k=3)
    top3.__name__ = 'top3_categorical_accuracy'
    model = Model(inputs=model_old.inputs, outputs=new_output)
    model.compile(optimizer='adam', loss='categorical_crossentropy',
                  metrics=['accuracy', 'categorical_accuracy', top3, metrics.mae,
                           metrics.mse])
    earlystop = callbacks.EarlyStopping(monitor='val_acc', min_delta=0, patience=0, verbose=1, mode='auto')
    time_start = time.time()
    model_history = model.fit(X_train, y_train,
                              batch_size=train_config.BATCH_SIZE,
                              epochs=train_config.NUM_EPOCHS,
                              shuffle=False,
                              callbacks=[earlystop],
                              validation_split=0.1
                              )

    model.save(CONVLSTM_IMG_GRAPH + "-over-new.h5")
