from trainconfig import expt_runner, TrainConfig

if __name__ == '__main__':
    sensor_train = 'fusion_file'
    train_config = TrainConfig()
    train_config.IMG_CHKPT_FILENAME = "results_img/chkpts/best_img_1511181849_aded_weights"
    train_config.ACC_CHKPT_FILENAME = "results_acc/chkpts/best_acc_1511183757_aded_weights"
    train_config.GYR_CHKPT_FILENAME = "results_gyr/chkpts/best_gyr_1511247849_aded_weights"
    train_config.MAG_CHKPT_FILENAME = "results_mag/chkpts/best_mag_1511252072_aded_weights"
    expt_runner(sensor_train, num_trials=10, num_epochs_hyperopt=10, k_folds=5, num_epochs_cross=100,
                train_config=train_config)
