from convert_keras_to_tf import convert_keras_to_tf_pb

convert_keras_to_tf_pb('results_fusion_file/chkpts/best_fusion_file_1509695739_aded_weights_combined.h5',
                       'results_fusion_file/chkpts/best_fusion_file_1509695739_aded_with_inception.pb',
                       'pred_fusion/Softmax')
