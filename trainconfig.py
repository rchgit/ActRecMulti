RANDOM_SEED = 42
import itertools
import os
import pickle
import sys
import time
import random as rn
from functools import partial

import imageio
import matplotlib
import numpy as np
import tensorflow as tf
from hyperopt import fmin, tpe, hp, Trials, space_eval, STATUS_OK

np.random.seed(RANDOM_SEED)
tf.set_random_seed(RANDOM_SEED)
rn.seed(RANDOM_SEED)
from keras import backend as K
from keras import metrics, regularizers, Input, callbacks
from keras.engine import Model
from keras.layers import Conv1D, LSTM, Dropout, Dense, Concatenate
from keras.models import model_from_json, load_model
from keras.utils import to_categorical, plot_model
from sklearn import preprocessing
from sklearn.metrics import average_precision_score, precision_recall_fscore_support, confusion_matrix
from sklearn.model_selection import train_test_split, StratifiedKFold

from convert_keras_to_tf import freeze_session, display_nodes
from logger import Logger
from multi_gpu_keras import to_multi_gpu
from parallelizer import Parallelizer


# matplotlib.rcParams.update({'backend': 'Qt5Agg', 'font.size': 18})


def get_labels(labels_filename=None, data_path=None, sensor_train=None):
    if labels_filename is None and data_path is not None and sensor_train is not None:
        LABELS = ([x for x in os.listdir(data_path + "/" + sensor_train)
                   if os.path.isdir(data_path + "/" + sensor_train + "/" + x)])
        with open('har_labels.txt', 'w') as labels_file:
            labels_file.write("\n".join(LABELS))
    else:
        with open(labels_filename, 'r') as labels_file:
            LABELS = [line.strip('\n') for line in labels_file]
    return LABELS


def mkdir_conditional(foldername):
    if not os.path.exists(foldername):
        try:
            os.makedirs(foldername)
        except OSError:
            pass

import matplotlib.pyplot as plt

try:
    with open('error_videos.txt', 'r') as file:
        error_video_filenames = file.read().split("\n")
except IOError:
    error_video_filenames = []
    print("No error video file list yet, creating one.")

MULTI_GPU = False
N_GPUS = 8

sensor_fullname = {
    'img': "Video",
    'acc': "Accelerometer",
    'gyr': "Gyroscope",
    'mag': "Magnetometer",
    'fusion_file': "Fusion"
}


def logger_init(filename):
    return Logger(filename)





hyperspace = {
    # 'seq_length': hp.quniform('seq_length', 75, 125, 1),
    # 'seq_stride': hp.quniform('seq_stride', 1, 10, 1),
    # 'seq_length': hp.quniform('seq_length', 120, 120,1),
    # 'seq_stride': hp.quniform('seq_stride', 1, 10, 1),
    'cnn_1':
        {
            'cnn_1_size_filters': hp.quniform('cnn_1_size_filters', 2, 5, 1),
            'cnn_1_num_filters': hp.quniform('cnn_1_num_filters', 2, 64, 1),
            # 'cnn_1_l2_strength': hp.loguniform('cnn_1_l2_strength', np.log(0.001), np.log(0.1)),

            'cnn_2': hp.choice('cnn_2', [
                False,
                {
                    'cnn_2_size_filters': hp.quniform('cnn_2_size_filters', 2, 5, 1),
                    'cnn_2_num_filters': hp.quniform('cnn_2_num_filters', 2, 64, 1),
                    # 'cnn_2_l2_strength': hp.loguniform('cnn_2_l2_strength', np.log(0.001), np.log(0.1)),

                    'cnn_3': hp.choice('cnn_3', [
                        False,
                        {
                            'cnn_3_size_filters': hp.quniform('cnn_3_size_filters', 2, 5, 1),
                            'cnn_3_num_filters': hp.quniform('cnn_3_num_filters', 2, 64, 1),
                            # 'cnn_3_l2_strength': hp.loguniform('cnn_3_l2_strength', np.log(0.001), np.log(0.1)),
                            'cnn_4': hp.choice('cnn_4', [
                                False,
                                {
                                    'cnn_4_size_filters': hp.quniform('cnn_4_size_filters', 2, 5, 1),
                                    'cnn_4_num_filters': hp.quniform('cnn_4_num_filters', 2, 64, 1),
                                    # 'cnn_4_l2_strength': hp.loguniform('cnn_4_l2_strength', np.log(0.001),
                                    #                                    np.log(0.1))

                                }
                            ]),
                        },

                    ]),

                }
            ]),

        }
    ,

    'lstm_last':
        {
            'lstm_last_num_cells': hp.quniform('lstm_last_num_cells', 1, 128, 1),
            # 'lstm_last_l2_strength': hp.loguniform('lstm_last_l2_strength', np.log(0.001), np.log(0.1))
        }
    ,
    'lstm_1': hp.choice('lstm_1', [
        False,
        {
            'lstm_1_num_cells': hp.quniform('lstm_1_num_cells', 1, 128, 1),
            # 'lstm_1_l2_strength': hp.loguniform('lstm_1_l2_strength', np.log(0.001), np.log(0.1)),

            'lstm_2': hp.choice('lstm_2', [
                False,
                {
                    'lstm_2_num_cells': hp.quniform('lstm_2_num_cells', 1, 128, 1),
                    # 'lstm_2_l2_strength': hp.loguniform('lstm_2_l2_strength', np.log(0.001), np.log(0.1)),

                    'lstm_3': hp.choice('lstm_3', [
                        False,
                        {
                            'lstm_3_num_cells': hp.quniform('lstm_3_num_cells', 1, 128, 1),
                            # 'lstm_3_l2_strength': hp.loguniform('lstm_3_l2_strength', np.log(0.001),
                            #                                     np.log(0.1))

                        }
                    ]),
                }
            ]),

        },

    ]),

    # 'dense_l2_strength': hp.loguniform('dense_l2_strength', np.log(0.001), np.log(0.1)),
    # 'dropout': hp.choice('dropout', [
    #     False,
    #     {
    #
    #     }
    # ]),
    'dropout_prob': hp.loguniform('dropout_prob', np.log(0.1), np.log(0.5))
    # 'batch_size': hp.choice('batch_size', [64, 128, 256, 512, 1024]),
}
hyperspace_fusion_file = {
    'dense_1': hp.choice('dense_1', [
        False,
        {
            'dense_1_num_units': hp.quniform('dense_1_num_units', 1, 20, 1),
            # 'dense_1_l2_strength': hp.loguniform('dense_1_l2_strength', np.log(0.001),
            #                                      np.log(0.1))
        }
    ]),
    # 'dense_last':
    #     {
    #         'dense_last_l2_strength': hp.loguniform('dense_last_l2_strength', np.log(0.001),
    #                                                 np.log(0.1))
    #     },
    # 'batch_size': hp.choice('batch_size', [64, 128, 256, 512, 1024])
    'dropout_prob': hp.loguniform('dropout_prob', np.log(0.1), np.log(0.5))
}
mead_sensor_cols = {
    'acc': slice(0, 3),
    'gyr': slice(6, 9),
    'mag': slice(12, 15)
}

hyperspace_fusion_full = {
    # 'seq_length': hp.quniform('seq_length', 10, 100, 1),
    # 'seq_stride': hp.quniform('seq_stride', 10, 15, 1),
    'img': {

        'img_lstm_last':
            {
                'img_lstm_last_num_cells': hp.quniform('img_lstm_last_num_cells', 1, 128, 1),
                # 'img_lstm_last_l2_strength': hp.loguniform('img_lstm_last_l2_strength', np.log(0.001), np.log(0.1))
            }
        ,
        'img_lstm_1': hp.choice('img_lstm_1', [
            False,
            {
                'img_lstm_1_num_cells': hp.quniform('img_lstm_1_num_cells', 1, 128, 1),
                # 'img_lstm_1_l2_strength': hp.loguniform('img_lstm_1_l2_strength', np.log(0.001), np.log(0.1)),

                'img_lstm_2': hp.choice('img_lstm_2', [
                    False,
                    {
                        'img_lstm_2_num_cells': hp.quniform('img_lstm_2_num_cells', 1, 128, 1),
                        # 'img_lstm_2_l2_strength': hp.loguniform('img_lstm_2_l2_strength', np.log(0.001), np.log(0.1)),

                        'img_lstm_3': hp.choice('img_lstm_3', [
                            False,
                            {
                                'img_lstm_3_num_cells': hp.quniform('img_lstm_3_num_cells', 1, 128, 1),
                                # 'img_lstm_3_l2_strength': hp.loguniform('img_lstm_3_l2_strength', np.log(0.001),
                                #                                         np.log(0.1))

                            }
                        ]),
                    }
                ]),

            },

        ]),

        # 'img_dense_l2_strength': hp.loguniform('img_dense_l2_strength', np.log(0.001), np.log(0.1))
    },
    'acc': {

        'acc_cnn_1':
            {
                'acc_cnn_1_size_filters': hp.quniform('acc_cnn_1_size_filters', 2, 5, 1),
                'acc_cnn_1_num_filters': hp.quniform('acc_cnn_1_num_filters', 2, 64, 1),
                # 'acc_cnn_1_l2_strength': hp.loguniform('acc_cnn_1_l2_strength', np.log(0.001), np.log(0.1)),

                'acc_cnn_2': hp.choice('acc_cnn_2', [
                    False,
                    {
                        'acc_cnn_2_size_filters': hp.quniform('acc_cnn_2_size_filters', 2, 5, 1),
                        'acc_cnn_2_num_filters': hp.quniform('acc_cnn_2_num_filters', 2, 64, 1),
                        # 'acc_cnn_2_l2_strength': hp.loguniform('acc_cnn_2_l2_strength', np.log(0.001), np.log(0.1)),

                        'acc_cnn_3': hp.choice('acc_cnn_3', [
                            False,
                            {
                                'acc_cnn_3_size_filters': hp.quniform('acc_cnn_3_size_filters', 2, 5, 1),
                                'acc_cnn_3_num_filters': hp.quniform('acc_cnn_3_num_filters', 2, 64, 1),
                                # 'acc_cnn_3_l2_strength': hp.loguniform('acc_cnn_3_l2_strength', np.log(0.001),
                                #                                        np.log(0.1)),
                                'acc_cnn_4': hp.choice('acc_cnn_4', [
                                    False,
                                    {
                                        'acc_cnn_4_size_filters': hp.quniform('acc_cnn_4_size_filters', 2, 5, 1),
                                        'acc_cnn_4_num_filters': hp.quniform('acc_cnn_4_num_filters', 2, 64, 1),
                                        # 'acc_cnn_4_l2_strength': hp.loguniform('acc_cnn_4_l2_strength', np.log(0.001),
                                        #                                        np.log(0.1))

                                    }
                                ]),
                            },

                        ]),

                    }
                ]),

            }
        ,
        'acc_lstm_last':
            {
                'acc_lstm_last_num_cells': hp.quniform('acc_lstm_last_num_cells', 1, 128, 1),
                # 'acc_lstm_last_l2_strength': hp.loguniform('acc_lstm_last_l2_strength', np.log(0.001), np.log(0.1))
            }
        ,
        'acc_lstm_1': hp.choice('acc_lstm_1', [
            False,
            {
                'acc_lstm_1_num_cells': hp.quniform('acc_lstm_1_num_cells', 1, 128, 1),
                # 'acc_lstm_1_l2_strength': hp.loguniform('acc_lstm_1_l2_strength', np.log(0.001), np.log(0.1)),

                'acc_lstm_2': hp.choice('acc_lstm_2', [
                    False,
                    {
                        'acc_lstm_2_num_cells': hp.quniform('acc_lstm_2_num_cells', 1, 128, 1),
                        # 'acc_lstm_2_l2_strength': hp.loguniform('acc_lstm_2_l2_strength', np.log(0.001), np.log(0.1)),

                        'acc_lstm_3': hp.choice('acc_lstm_3', [
                            False,
                            {
                                'acc_lstm_3_num_cells': hp.quniform('acc_lstm_3_num_cells', 1, 128, 1),
                                # 'acc_lstm_3_l2_strength': hp.loguniform('acc_lstm_3_l2_strength', np.log(0.001),
                                #                                         np.log(0.1))

                            }
                        ]),
                    }
                ]),

            },

        ]),

        # 'acc_dense_l2_strength': hp.loguniform('acc_dense_l2_strength', np.log(0.001), np.log(0.1))
    },
    'gyr': {
        'gyr_cnn_1':
            {
                'gyr_cnn_1_size_filters': hp.quniform('gyr_cnn_1_size_filters', 2, 5, 1),
                'gyr_cnn_1_num_filters': hp.quniform('gyr_cnn_1_num_filters', 2, 64, 1),
                # 'gyr_cnn_1_l2_strength': hp.loguniform('gyr_cnn_1_l2_strength', np.log(0.001), np.log(0.1)),

                'gyr_cnn_2': hp.choice('gyr_cnn_2', [
                    False,
                    {
                        'gyr_cnn_2_size_filters': hp.quniform('gyr_cnn_2_size_filters', 2, 5, 1),
                        'gyr_cnn_2_num_filters': hp.quniform('gyr_cnn_2_num_filters', 2, 64, 1),
                        # 'gyr_cnn_2_l2_strength': hp.loguniform('gyr_cnn_2_l2_strength', np.log(0.001), np.log(0.1)),

                        'gyr_cnn_3': hp.choice('gyr_cnn_3', [
                            False,
                            {
                                'gyr_cnn_3_size_filters': hp.quniform('gyr_cnn_3_size_filters', 2, 5, 1),
                                'gyr_cnn_3_num_filters': hp.quniform('gyr_cnn_3_num_filters', 2, 64, 1),
                                # 'gyr_cnn_3_l2_strength': hp.loguniform('gyr_cnn_3_l2_strength', np.log(0.001),
                                #                                        np.log(0.1)),
                                'gyr_cnn_4': hp.choice('gyr_cnn_4', [
                                    False,
                                    {
                                        'gyr_cnn_4_size_filters': hp.quniform('gyr_cnn_4_size_filters', 2, 5, 1),
                                        'gyr_cnn_4_num_filters': hp.quniform('gyr_cnn_4_num_filters', 2, 64, 1),
                                        # 'gyr_cnn_4_l2_strength': hp.loguniform('gyr_cnn_4_l2_strength', np.log(0.001),
                                        #                                        np.log(0.1))

                                    }
                                ]),
                            },

                        ]),

                    }
                ]),

            }
        ,
        'gyr_lstm_last':
            {
                'gyr_lstm_last_num_cells': hp.quniform('gyr_lstm_last_num_cells', 1, 128, 1),
                # 'gyr_lstm_last_l2_strength': hp.loguniform('gyr_lstm_last_l2_strength', np.log(0.001), np.log(0.1))
            }
        ,
        'gyr_lstm_1': hp.choice('gyr_lstm_1', [
            False,
            {
                'gyr_lstm_1_num_cells': hp.quniform('gyr_lstm_1_num_cells', 1, 128, 1),
                # 'gyr_lstm_1_l2_strength': hp.loguniform('gyr_lstm_1_l2_strength', np.log(0.001), np.log(0.1)),

                'gyr_lstm_2': hp.choice('gyr_lstm_2', [
                    False,
                    {
                        'gyr_lstm_2_num_cells': hp.quniform('gyr_lstm_2_num_cells', 1, 128, 1),
                        # 'gyr_lstm_2_l2_strength': hp.loguniform('gyr_lstm_2_l2_strength', np.log(0.001), np.log(0.1)),

                        'gyr_lstm_3': hp.choice('gyr_lstm_3', [
                            False,
                            {
                                'gyr_lstm_3_num_cells': hp.quniform('gyr_lstm_3_num_cells', 1, 128, 1),
                                # 'gyr_lstm_3_l2_strength': hp.loguniform('gyr_lstm_3_l2_strength', np.log(0.001),
                                #                                         np.log(0.1))

                            }
                        ]),
                    }
                ]),

            },

        ]),

        # 'gyr_dense_l2_strength': hp.loguniform('gyr_dense_l2_strength', np.log(0.001), np.log(0.1))
    },
    'mag': {
        # 'mag_seq_length': hp.quniform('mag_seq_length', 75, 125, 1),
        # 'mag_seq_stride': hp.quniform('mag_seq_stride', 1, 10, 1),

        'mag_cnn_1':
            {
                'mag_cnn_1_size_filters': hp.quniform('mag_cnn_1_size_filters', 2, 5, 1),
                'mag_cnn_1_num_filters': hp.quniform('mag_cnn_1_num_filters', 2, 64, 1),
                # 'mag_cnn_1_l2_strength': hp.loguniform('mag_cnn_1_l2_strength', np.log(0.001), np.log(0.1)),

                'mag_cnn_2': hp.choice('mag_cnn_2', [
                    False,
                    {
                        'mag_cnn_2_size_filters': hp.quniform('mag_cnn_2_size_filters', 2, 5, 1),
                        'mag_cnn_2_num_filters': hp.quniform('mag_cnn_2_num_filters', 2, 64, 1),
                        # 'mag_cnn_2_l2_strength': hp.loguniform('mag_cnn_2_l2_strength', np.log(0.001), np.log(0.1)),

                        'mag_cnn_3': hp.choice('mag_cnn_3', [
                            False,
                            {
                                'mag_cnn_3_size_filters': hp.quniform('mag_cnn_3_size_filters', 2, 5, 1),
                                'mag_cnn_3_num_filters': hp.quniform('mag_cnn_3_num_filters', 2, 64, 1),
                                # 'mag_cnn_3_l2_strength': hp.loguniform('mag_cnn_3_l2_strength', np.log(0.001),
                                #                                        np.log(0.1)),
                                'mag_cnn_4': hp.choice('mag_cnn_4', [
                                    False,
                                    {
                                        'mag_cnn_4_size_filters': hp.quniform('mag_cnn_4_size_filters', 2, 5, 1),
                                        'mag_cnn_4_num_filters': hp.quniform('mag_cnn_4_num_filters', 2, 64, 1),
                                        # 'mag_cnn_4_l2_strength': hp.loguniform('mag_cnn_4_l2_strength', np.log(0.001),
                                        #                                        np.log(0.1))

                                    }
                                ]),
                            },

                        ]),

                    }
                ]),

            }
        ,
        'mag_lstm_last':
            {
                'mag_lstm_last_num_cells': hp.quniform('mag_lstm_last_num_cells', 1, 128, 1),
                # 'mag_lstm_last_l2_strength': hp.loguniform('mag_lstm_last_l2_strength', np.log(0.001), np.log(0.1))
            }
        ,
        'mag_lstm_1': hp.choice('mag_lstm_1', [
            False,
            {
                'mag_lstm_1_num_cells': hp.quniform('mag_lstm_1_num_cells', 1, 128, 1),
                # 'mag_lstm_1_l2_strength': hp.loguniform('mag_lstm_1_l2_strength', np.log(0.001), np.log(0.1)),

                'mag_lstm_2': hp.choice('mag_lstm_2', [
                    False,
                    {
                        'mag_lstm_2_num_cells': hp.quniform('mag_lstm_2_num_cells', 1, 128, 1),
                        # 'mag_lstm_2_l2_strength': hp.loguniform('mag_lstm_2_l2_strength', np.log(0.001), np.log(0.1)),

                        'mag_lstm_3': hp.choice('mag_lstm_3', [
                            False,
                            {
                                'mag_lstm_3_num_cells': hp.quniform('mag_lstm_3_num_cells', 1, 128, 1),
                                # 'mag_lstm_3_l2_strength': hp.loguniform('mag_lstm_3_l2_strength', np.log(0.001),
                                #                                         np.log(0.1))

                            }
                        ]),
                    }
                ]),

            },

        ]),

        # 'mag_dense_l2_strength': hp.loguniform('mag_dense_l2_strength', np.log(0.001), np.log(0.1))
    },

}

hyperspace_fusion_full.update({'dense_1': hp.choice('dense_1', [
    False,
    {
        'dense_1_num_units': hp.quniform('dense_1_num_units', 1, 20, 1),
        'dense_1_l2_strength': hp.loguniform('dense_1_l2_strength', np.log(0.001),
                                             np.log(0.1))
    }
]),
                               'dense_last':
                                   {
                                       'dense_last_l2_strength': hp.loguniform('dense_last_l2_strength', np.log(0.001),
                                                                               np.log(0.1))
                                   },
                               'batch_size': hp.choice('batch_size', [64, 128, 256, 512, 1024])
                               }
                              )


# set_session(sess)


# noinspection PyCompatibility
class TrainConfig:
    def __init__(self):
        super().__init__()
        self.SEQ_LENGTH = 0
        self.SEQ_STRIDE = 0
        self.FEAT_VEC_LENGTH = 3
        self.NUM_EPOCHS = 1
        self.TEST_RATIO = 0.1
        self.VALID_RATIO = 0.1111111
        self.RANDOM_STATE = 42
        self.BATCH_SIZE = 1024
        self.FINAL_TENSOR_NAME = 'softmax/Softmax'
        self.DATA_PATH = "dataset_aded"
        self.LABELS_FILE = ''
        self.SENSOR_TRAIN = ''
        self.FEAT_EXTRACTOR_NAME = "inception_v1_slim_frozen.pb"
        self.POOL_TENSOR_NAME = 'InceptionV1/Logits/MaxPool_0a_7x7/AvgPool:0'
        self.CNN_INPUT_TENSOR_NAME = 'Cast:0'
        self.VIDEO_READER = "FFMPEG"
        self.EXPT_FILENAME = ""
        self.CHECKPOINTS_DIR = ""
        self.IMG_CHKPT_FILENAME = ""
        self.ACC_CHKPT_FILENAME = ""
        self.GYR_CHKPT_FILENAME = ""
        self.MAG_CHKPT_FILENAME = ""


def rebuild_model(model_string, MULTI_GPU=False):
    # create model
    model = model_from_json(model_string)
    if MULTI_GPU:
        model = to_multi_gpu(model, N_GPUS)
    # Compile model
    model.compile(optimizer='adam', loss='categorical_crossentropy',
                  metrics=['accuracy', 'categorical_accuracy', 'top_k_categorical_accuracy', metrics.mae,
                           metrics.mse])

    return model


def display_confusion(confusion, LABELS, sensor, show=False, phase='test', normalized=True):
    phase_dict = {'train': "Training", "test": "Testing", "valid": "Validation"}
    normalized_dict = {True: "Normalized", False: ""}
    normalized_decimal = {True: "{0:.2f}", False: "{:d}"}
    width = 12
    height = 12
    fig = plt.figure(figsize=(width, height))
    tick_marks = np.arange(len(LABELS))
    plt.xticks(tick_marks, LABELS, rotation=45, figure=fig)
    plt.yticks(tick_marks, LABELS, figure=fig)
    plt.imshow(
        confusion,
        interpolation='nearest',
        cmap=plt.cm.Blues,
        figure=fig
    )
    if sensor in ['fusion_full', 'fusion_file']:
        plt.title("Fusion " + " " + phase_dict[phase] + " " + normalized_dict[normalized] + " Confusion Matrix", figure=fig)
    else:
        plt.title(
            sensor_fullname[sensor] + " " + phase_dict[phase] + " " + normalized_dict[normalized] + " Confusion Matrix",
            figure=fig)
    thresh = confusion.max() / 2.
    for i, j in itertools.product(range(confusion.shape[0]), range(confusion.shape[1])):
        plt.text(j, i, normalized_decimal[normalized].format(confusion[i, j]),
                 horizontalalignment="center",
                 color="white" if confusion[i, j] > thresh else "black",
                 figure=fig)
    plt.colorbar()
    plt.tight_layout()
    plt.ylabel('True label', figure=fig)
    plt.xlabel('Predicted label', figure=fig)
    if show: plt.show()
    return fig





def data_aded(train_config, crossval=False):
    scaler = preprocessing.MinMaxScaler()
    sensor_train = train_config.SENSOR_TRAIN

    if train_config.SENSOR_TRAIN == 'img':
        train_config.FEAT_VEC_LENGTH = 1024
    else:
        train_config.FEAT_VEC_LENGTH = 3

    num_unique_samples = 0
    print('Sequence length: ' + str(train_config.SEQ_LENGTH))
    print('Sequence stride: ' + str(train_config.SEQ_STRIDE))
    LABELS = get_labels(train_config.LABELS_FILE, train_config.DATA_PATH, train_config.SENSOR_TRAIN)

    print(LABELS)
    NUM_CLASSES = len(LABELS)
    if sensor_train == 'img':
        train_config.FEAT_CACHE_PATH = "features_img"

        for label in LABELS:
            mkdir_conditional(train_config.FEAT_CACHE_PATH + "/" + label)

        # Data acquisition
        lstm_features = []
        lstm_classes = []

        # with tf.Session(config=config) as sess:
        with tf.gfile.FastGFile(train_config.FEAT_EXTRACTOR_NAME, 'rb') as fin:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(fin.read())
            _ = tf.import_graph_def(graph_def, name='')
        for class_index, label in enumerate(LABELS):
            for _, _, files in os.walk(train_config.DATA_PATH + "/" + sensor_train + "/" + label):
                for filename in files:
                    lstm_feature_list = []
                    # Read bottleneck cache
                    cnn_feat_filename = train_config.FEAT_CACHE_PATH + "/" + label + "/" + filename + ".feature"
                    if os.path.isfile(cnn_feat_filename):
                        with open(cnn_feat_filename, 'rb') as cnn_feat_file:
                            cnn_feats = pickle.load(cnn_feat_file)

                    else:
                        try:
                            sess = K.get_session()
                            full_filename = train_config.DATA_PATH + "/" + sensor_train + "/" + label + "/" + filename
                            # For each frame
                            with imageio.get_reader(full_filename, format=train_config.VIDEO_READER) as reader:
                                print("Acquiring: " + full_filename)
                                cnn_feats = []

                                try:
                                    for frame_index, frame in enumerate(reader):
                                        # if not (frame_index % int(reader._meta['fps'])):
                                        # NOTE: should I add decimation here (e.g. every 10 frames?)
                                        # Run Inception inference
                                        pool_tensor = sess.graph.get_tensor_by_name(train_config.POOL_TENSOR_NAME)
                                        # Add to queue
                                        cnn_feat = sess.run(pool_tensor, {train_config.CNN_INPUT_TENSOR_NAME:
                                                                              frame})
                                        cnn_feats.append(np.squeeze(cnn_feat))
                                except imageio.core.format.CannotReadFrameError:
                                    print("Erroneous last frame.")
                                    bare_filename, _ = os.path.splitext(filename)
                                    error_video_filenames.append(bare_filename)

                                cnn_feats = np.array(cnn_feats)
                                try:
                                    with open(cnn_feat_filename, 'wb') as cnn_feat_file:
                                        pickle.dump(cnn_feats, cnn_feat_file)
                                        print("Saving " + cnn_feat_filename)
                                except IOError:
                                    print("IOError occurred while saving cache")

                        except Exception as e:
                            print(str(type(e)) + ': ' + str(e))
                            continue

                    for i in range(0, cnn_feats.shape[0], train_config.SEQ_STRIDE):
                        sequence = cnn_feats[i:i + train_config.SEQ_LENGTH, :]
                        if np.size(sequence, 0) == train_config.SEQ_LENGTH:
                            lstm_feature_list.append(sequence)
                    try:
                        assert len(lstm_feature_list) > 0
                    except AssertionError:
                        # print("len(lstm_feature_list) <= 0")
                        continue
                    lstm_features.extend(lstm_feature_list)
                    lstm_classes.append(np.full(len(lstm_feature_list), class_index))

        lstm_features = np.rollaxis(np.dstack(lstm_features), axis=-1)
        lstm_classes = np.array([item for sublist in lstm_classes for item in sublist])

        if not crossval:
            lstm_classes = to_categorical(lstm_classes, NUM_CLASSES)
            # Split into train and validation.
            X_train, X_test, y_train, y_test = train_test_split(
                lstm_features, lstm_classes, test_size=train_config.TEST_RATIO,
                random_state=train_config.RANDOM_STATE)
            X_train, X_valid, y_train, y_valid = train_test_split(
                X_train, y_train, test_size=train_config.VALID_RATIO,
                random_state=train_config.RANDOM_STATE)
            return X_train, X_valid, X_test, y_train, y_valid, y_test, NUM_CLASSES
        else:
            return lstm_features, lstm_classes, LABELS
    else:
        # Data preprocessing
        for class_index, label in enumerate(LABELS):
            for root, dirs, files in os.walk(train_config.DATA_PATH + "/" + train_config.SENSOR_TRAIN + "/" + label):
                for filename in files:
                    # Read csv into numpy array
                    if filename.endswith('.csv'):
                        try:
                            signal_array = np.genfromtxt(
                                train_config.DATA_PATH + "/" + train_config.SENSOR_TRAIN + "/" + label + "/" + filename,
                                dtype=np.float32,
                                delimiter=',')
                            signal_array = signal_array[:, 1:4]
                            scaler.partial_fit(signal_array)
                            num_unique_samples += len(
                                range(0, np.size(signal_array, axis=0) - train_config.SEQ_LENGTH + 1,
                                      train_config.SEQ_STRIDE))
                        except Exception as e:
                            print(e)
                            continue

        # Data acquisition
        lstm_features = []
        lstm_classes = []
        for class_index, label in enumerate(LABELS):
            # print('Class index: ' + str(class_index))
            for root, dirs, files in os.walk(train_config.DATA_PATH + "/" + train_config.SENSOR_TRAIN + "/" + label):
                for filename in files:
                    # Read csv into numpy array
                    if filename.endswith('.csv'):
                        try:
                            signal_array = np.genfromtxt(
                                train_config.DATA_PATH + "/" + train_config.SENSOR_TRAIN + "/" + label + "/" + filename,
                                dtype=np.float32,
                                delimiter=',')
                            signal_array = signal_array[:, 1:4]
                            signal_array = scaler.transform(signal_array)

                            for i in range(0, np.size(signal_array, axis=0), train_config.SEQ_STRIDE):
                                # Add to the queue.
                                # In order to turn our discrete predictions or features into a sequence,
                                # we loop through each frame in chronological order, add it to a queue of size N,
                                # and pop off the first frame we previously added.
                                # N represents the length of our sequence that we'll pass to the RNN.
                                sequence = signal_array[i:i + train_config.SEQ_LENGTH, :]
                                # Zero-padding if sequence is shorter than prescribed length
                                if sequence.shape[0] < train_config.SEQ_LENGTH:
                                    sequence = np.append(sequence,
                                                         np.zeros(
                                                             [train_config.SEQ_LENGTH - sequence.shape[0],
                                                              train_config.FEAT_VEC_LENGTH]),
                                                         axis=0)
                                lstm_features.append(sequence)
                                lstm_classes.append(class_index)

                        except Exception as e:
                            print(e)
                            continue

        # Numpy.
        lstm_features = np.rollaxis(np.dstack(lstm_features), axis=-1)

        MIN = scaler.data_min_
        RANGE = scaler.data_range_
        print(sensor_train + ' Min: ' + str(MIN))
        print(sensor_train + ' Range: ' + str(RANGE))
        # Split into train and validation.

        if not crossval:
            lstm_classes = to_categorical(lstm_classes, NUM_CLASSES)
            X_train, X_test, y_train, y_test = train_test_split(
                lstm_features, lstm_classes, test_size=train_config.TEST_RATIO,
                random_state=train_config.RANDOM_STATE)
            X_train, X_valid, y_train, y_valid = train_test_split(
                X_train, y_train, test_size=train_config.VALID_RATIO,
                random_state=train_config.RANDOM_STATE)
            return X_train, X_valid, X_test, y_train, y_valid, y_test, MIN, RANGE, NUM_CLASSES
        else:
            return lstm_features, lstm_classes, MIN, RANGE, LABELS, NUM_CLASSES


def data_aded_fusion_file(train_config, crossval=False, sensor_select=None):
    sess = K.get_session()
    sensor_list = ['img', 'acc', 'gyr', 'mag']
    sensor_list_mech = ['acc', 'gyr', 'mag']
    if sensor_select is not None and sensor_select in sensor_list:
        scaler = preprocessing.MinMaxScaler()
    else:
        scaler = {'acc': preprocessing.MinMaxScaler(),
                  'gyr': preprocessing.MinMaxScaler(),
                  'mag': preprocessing.MinMaxScaler(),
                  }

    LABELS = get_labels(train_config.LABELS_FILE, train_config.DATA_PATH, train_config.SENSOR_TRAIN)

    print(LABELS)
    NUM_CLASSES = len(LABELS)

    train_config.FEAT_CACHE_PATH = 'features_fusion'
    #
    # for x in LABELS:
    #     mkdir_conditional(train_config.FEAT_CACHE_PATH + x)
    # Data acquisition
    if sensor_select is not None and sensor_select in sensor_list:
        lstm_features = {sensor_select: []}
    else:
        lstm_features = {key: [] for key in sensor_list}
    lstm_classes = []

    with tf.gfile.FastGFile(train_config.FEAT_EXTRACTOR_NAME, 'rb') as fin:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(fin.read())
        _ = tf.import_graph_def(graph_def, name='')
    signal_array_interp_list = []
    for class_index, label in enumerate(LABELS):

        for ((_, _, files_img), (_, _, files_acc), (_, _, files_gyr), (_, _, files_mag)) \
                in zip(os.walk(train_config.DATA_PATH + "/img/" + label),
                       os.walk(train_config.DATA_PATH + "/acc/" + label),
                       os.walk(train_config.DATA_PATH + "/gyr/" + label),
                       os.walk(train_config.DATA_PATH + "/mag/" + label),
                       ):
            for (filename_img, filename_acc, filename_gyr, filename_mag) \
                    in zip(files_img, files_acc, files_gyr, files_mag):
                bare_filename, _ = os.path.splitext(filename_img)
                try:
                    # Check if there's a fusion raw data file existing

                    if bare_filename in error_video_filenames:
                        raise Exception('Video file has errors...skipping...')
                    full_filename = train_config.FEAT_CACHE_PATH + '/' + label + '/' + bare_filename + '.fusion'
                    if os.path.exists(full_filename):
                        with open(full_filename, 'rb') as f:
                            signal_array_interp = pickle.load(f)
                            if sensor_select is not None:
                                signal_array_interp_list.append(signal_array_interp[sensor_select])
                            else:
                                signal_array_interp_list.append(signal_array_interp)

                            signal_array_interp = {key: None for key in sensor_list}
                        continue
                except Exception as e:
                    print(type(e))
                    print(e)
                    continue

                # If no fusion file, begin acquisition of video frames
                try:

                    assert filename_img.endswith('.mp4')
                    img_full_filename = train_config.DATA_PATH + "/img/" + label + "/" + filename_img
                    # For each frame
                    with imageio.get_reader(img_full_filename, format=train_config.VIDEO_READER) as reader:
                        print("Acquiring: " + img_full_filename)
                        meta_data = reader.get_meta_data()
                        frame_period = meta_data['fps'] / meta_data['nframes']
                        x_interp = frame_period * np.arange(0, meta_data['nframes'])
                        cnn_feat_list = []

                        for frame_index, frame in enumerate(reader):
                            # if not (frame_index % int(reader._meta['fps'])):
                            # NOTE: should I add decimation here (e.g. every 10 frames?)
                            # Run Inception inference
                            pool_tensor = sess.graph.get_tensor_by_name(train_config.POOL_TENSOR_NAME)
                            # Add to queue
                            cnn_feat = sess.run(pool_tensor, {train_config.CNN_INPUT_TENSOR_NAME:
                                                                  frame})
                            cnn_feat_list.append(np.squeeze(cnn_feat))

                        if sensor_select == 'img':
                            signal_array_interp = np.array(cnn_feat_list)
                        else:
                            signal_array_interp['img'] = np.array(cnn_feat_list)

                except imageio.core.format.CannotReadFrameError as cannot:
                    print(type(cannot))
                    print(cannot)
                    error_video_filenames.append(bare_filename)
                    continue
                except Exception as e:
                    print(e)
                    continue
                try:
                    # Begin sensor acquisitions
                    if sensor_select in sensor_list_mech:
                        # assert filename_acc.endswith('.csv')
                        signal_array = np.genfromtxt(
                            train_config.DATA_PATH + "/" + sensor_select + "/" + label + "/" + bare_filename + '.csv',
                            dtype=np.float32, delimiter=',')
                        signal_array = signal_array[:, 1:4]
                        y_interp_axis = []
                        for i in range(signal_array.shape[1]):
                            y_interp_axis.append(np.interp(x_interp, np.arange(0, signal_array.shape[0]),
                                                           signal_array[:, i]))
                        y_interp_axis = np.array(y_interp_axis)

                        signal_array_interp = y_interp_axis.T

                    else:
                        for sensor_train in sensor_list_mech:
                            # Read csv into numpy array

                            signal_array = np.genfromtxt(
                                train_config.DATA_PATH + "/" + sensor_train + "/" + label + "/" + bare_filename + ".csv",
                                dtype=np.float32, delimiter=',')
                            signal_array = signal_array[:, 1:4]
                            y_interp_axis = []
                            for i in range(signal_array.shape[1]):
                                y_interp_axis.append(np.interp(x_interp, np.arange(0, signal_array.shape[0]),
                                                               signal_array[:, i]))
                            y_interp_axis = np.array(y_interp_axis)

                            signal_array_interp[sensor_train] = y_interp_axis.T

                    # signal_array_interp_list.append(signal_array_interp)
                    final_filename, _ = os.path.splitext(filename_img)
                    mkdir_conditional('features_fusion/' + label)
                    with open('features_fusion/' + label + '/' + final_filename + '.fusion', 'wb') as f:
                        pickle.dump(signal_array_interp, f)


                except Exception as e:
                    print(str(type(e)) + ': ' + str(e))
                    continue

        for index, item in enumerate(signal_array_interp_list):
            if sensor_select is not None and sensor_select != 'img':
                scaler.partial_fit(item)
            elif sensor_select == 'img':
                break
            else:
                for sensor in sensor_list_mech:
                    scaler[sensor].partial_fit(item[sensor])
        for index, item in enumerate(signal_array_interp_list):
            if sensor_select is not None and sensor_select in sensor_list_mech:
                signal_array_interp_list[index] = scaler.transform(item)
            elif sensor_select == 'img':
                break
            else:
                for sensor in sensor_list_mech:
                    signal_array_interp_list[index][sensor] = scaler[sensor].transform(item[sensor])
    with open('error_videos.txt', 'w') as file:
        file.write("\n".join(error_video_filenames))

    for class_index, label in enumerate(LABELS):
        for signal_index, item in enumerate(signal_array_interp_list):
            try:
                assert all(v is not None for v in item)
                if sensor_select is not None:
                    num_samples_file = len(list(range(0, item.shape[0], train_config.SEQ_STRIDE)))

                    for i in range(num_samples_file):
                        # Add to the  queue.
                        # In order to turn our discrete predictions or features into a sequence,
                        # we loop through each frame in chronological order, add it to a queue of size N,
                        # and pop off the first frame we previously added.
                        # N represents the length of our sequence that we'll pass to the RNN.
                        if sensor_select == 'img':
                            feat_vec_length = 1024
                        else:
                            feat_vec_length = 3
                        sequence = item[i:i + train_config.SEQ_LENGTH, :]
                        # Zero-padding if sequence is shorter than prescribed length
                        if sequence.shape[0] < train_config.SEQ_LENGTH:
                            sequence = np.append(sequence,
                                                 np.zeros(
                                                     [train_config.SEQ_LENGTH - sequence.shape[0],
                                                      feat_vec_length]),
                                                 axis=0)
                        # lstm_feature.append(sequence)
                        lstm_features[sensor_select].append(sequence)
                    lstm_classes.append(np.full(num_samples_file, class_index))
                else:
                    num_samples_file = len(list(range(0, item['img'].shape[0], train_config.SEQ_STRIDE)))
                    for sensor in sensor_list:
                        # lstm_feature = []

                        for i in range(num_samples_file):
                            # Add to the  queue.
                            # In order to turn our discrete predictions or features into a sequence,
                            # we loop through each frame in chronological order, add it to a queue of size N,
                            # and pop off the first frame we previously added.
                            # N represents the length of our sequence that we'll pass to the RNN.
                            if sensor == 'img':
                                feat_vec_length = 1024
                            else:
                                feat_vec_length = 3
                            sequence = item[sensor][i:i + train_config.SEQ_LENGTH, :]
                            # Zero-padding if sequence is shorter than prescribed length
                            if sequence.shape[0] < train_config.SEQ_LENGTH:
                                sequence = np.append(sequence,
                                                     np.zeros(
                                                         [train_config.SEQ_LENGTH - sequence.shape[0],
                                                          feat_vec_length]),
                                                     axis=0)
                            # lstm_feature.append(sequence)
                            lstm_features[sensor].append(sequence)
                    lstm_classes.append(np.full(num_samples_file, class_index))
            except AssertionError as e:
                print(e)
                continue
            except MemoryError as e:
                print(type(e))
                print(e)

    X_train = {}
    X_test = {}
    X_valid = {}
    MIN = {}
    RANGE = {}
    print(len(lstm_classes))
    lstm_classes = np.concatenate(lstm_classes, axis=0)
    data_indices = np.arange(lstm_classes.shape[0], dtype=np.intp)
    if sensor_select is not None and sensor_select != 'img':
        MIN[sensor_select] = scaler.data_min_
        RANGE[sensor_select] = scaler.data_range_
    elif sensor_select == 'img':
        pass
    else:
        for sensor in sensor_list_mech:
            MIN[sensor] = scaler[sensor].data_min_
            RANGE[sensor] = scaler[sensor].data_range_

    if sensor_select is not None:
        lstm_features[sensor_select] = np.array(lstm_features[sensor_select])
        print(lstm_features[sensor_select].shape[0])
        assert lstm_features[sensor_select].shape[0] == lstm_classes.shape[0]
    else:
        for sensor in sensor_list:
            lstm_features[sensor] = np.array(lstm_features[sensor])
            print(lstm_features[sensor].shape[0])

            assert lstm_features[sensor].shape[0] == lstm_classes.shape[0]
    print(lstm_classes.shape[0])
    if not crossval:
        # with open('dataset_fusion.pkl', 'wb') as f:
        #     pickle.dump((NUM_CLASSES, X_train, X_valid, X_test, y_train, y_valid, y_test, MIN, RANGE), f)
        lstm_classes = to_categorical(lstm_classes, NUM_CLASSES)
        indices_train, indices_test, _, _ = train_test_split(
            data_indices, data_indices, test_size=train_config.TEST_RATIO,
            random_state=train_config.RANDOM_STATE)
        indices_train, indices_valid, _, _ = train_test_split(
            indices_train, indices_train, test_size=train_config.VALID_RATIO,
            random_state=train_config.RANDOM_STATE)

        y_train = lstm_classes[indices_train].astype(np.float32)
        y_test = lstm_classes[indices_test].astype(np.float32)
        y_valid = lstm_classes[indices_valid].astype(np.float32)

        if sensor_select is not None and sensor_select in sensor_list:
            X_train = lstm_features[sensor_select][indices_train]
            X_test = lstm_features[sensor_select][indices_test]
            X_valid = lstm_features[sensor_select][indices_valid]

            return X_train, X_valid, X_test, y_train, y_valid, y_test

        for key in lstm_features.keys():
            X_train[key] = lstm_features[key][indices_train]
            X_test[key] = lstm_features[key][indices_test]
            X_valid[key] = lstm_features[key][indices_valid]

        return X_train, X_valid, X_test, y_train, y_valid, y_test
    else:
        if sensor_select is not None and type(sensor_select) == str:
            assert sensor_select in sensor_list
            return lstm_features[sensor_select], lstm_classes[sensor_select]
        return lstm_features, lstm_classes


def data_aded_fusion(train_config, crossval=False, sensor_select=None):
    sess = K.get_session()
    sensor_list = ['img', 'acc', 'gyr', 'mag']
    sensor_list_mech = ['acc', 'gyr', 'mag']
    if sensor_select is not None and sensor_select in sensor_list:
        scaler = preprocessing.MinMaxScaler()
    else:
        scaler = {'acc': preprocessing.MinMaxScaler(),
                  'gyr': preprocessing.MinMaxScaler(),
                  'mag': preprocessing.MinMaxScaler(),
                  }

    LABELS = get_labels(train_config.LABELS_FILE, train_config.DATA_PATH, train_config.SENSOR_TRAIN)

    print(LABELS)
    NUM_CLASSES = len(LABELS)

    train_config.FEAT_CACHE_PATH = 'features_fusion'
    #
    # for x in LABELS:
    #     mkdir_conditional(train_config.FEAT_CACHE_PATH + x)
    # Data acquisition
    if sensor_select is not None and sensor_select in sensor_list:
        lstm_features = {sensor_select: []}
    else:
        lstm_features = {key: [] for key in sensor_list}
    lstm_classes = []

    with tf.gfile.FastGFile(train_config.FEAT_EXTRACTOR_NAME, 'rb') as fin:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(fin.read())
        _ = tf.import_graph_def(graph_def, name='')
    signal_array_interp_list = []
    for class_index, label in enumerate(LABELS):

        for ((_, _, files_img), (_, _, files_acc), (_, _, files_gyr), (_, _, files_mag)) \
                in zip(os.walk(train_config.DATA_PATH + "/img/" + label),
                       os.walk(train_config.DATA_PATH + "/acc/" + label),
                       os.walk(train_config.DATA_PATH + "/gyr/" + label),
                       os.walk(train_config.DATA_PATH + "/mag/" + label),
                       ):
            for (filename_img, filename_acc, filename_gyr, filename_mag) \
                    in zip(files_img, files_acc, files_gyr, files_mag):
                bare_filename, _ = os.path.splitext(filename_img)
                try:
                    # Check if there's a fusion raw data file existing

                    if bare_filename in error_video_filenames:
                        raise Exception('Video file has errors...skipping...')
                    full_filename = train_config.FEAT_CACHE_PATH + '/' + label + '/' + bare_filename + '.fusion'
                    if os.path.exists(full_filename):
                        with open(full_filename, 'rb') as f:
                            signal_array_interp = pickle.load(f)
                            if sensor_select is not None:
                                signal_array_interp_list.append(signal_array_interp[sensor_select])
                            else:
                                signal_array_interp_list.append(signal_array_interp)

                            signal_array_interp = {key: None for key in sensor_list}
                        continue
                except Exception as e:
                    print(type(e))
                    print(e)
                    continue

                # If no fusion file, begin acquisition of video frames
                try:

                    assert filename_img.endswith('.mp4')
                    img_full_filename = train_config.DATA_PATH + "/img/" + label + "/" + filename_img
                    # For each frame
                    with imageio.get_reader(img_full_filename, format=train_config.VIDEO_READER) as reader:
                        print("Acquiring: " + img_full_filename)
                        meta_data = reader.get_meta_data()
                        frame_period = meta_data['fps'] / meta_data['nframes']
                        x_interp = frame_period * np.arange(0, meta_data['nframes'])
                        cnn_feat_list = []

                        for frame_index, frame in enumerate(reader):
                            # if not (frame_index % int(reader._meta['fps'])):
                            # NOTE: should I add decimation here (e.g. every 10 frames?)
                            # Run Inception inference
                            pool_tensor = sess.graph.get_tensor_by_name(train_config.POOL_TENSOR_NAME)
                            # Add to queue
                            cnn_feat = sess.run(pool_tensor, {train_config.CNN_INPUT_TENSOR_NAME:
                                                                  frame})
                            cnn_feat_list.append(np.squeeze(cnn_feat))

                        if sensor_select == 'img':
                            signal_array_interp = np.array(cnn_feat_list)
                        else:
                            signal_array_interp['img'] = np.array(cnn_feat_list)

                except imageio.core.format.CannotReadFrameError as cannot:
                    print(type(cannot))
                    print(cannot)
                    error_video_filenames.append(bare_filename)
                    continue
                except Exception as e:
                    print(e)
                    continue
                try:
                    # Begin sensor acquisitions
                    if sensor_select in sensor_list_mech:
                        # assert filename_acc.endswith('.csv')
                        signal_array = np.genfromtxt(
                            train_config.DATA_PATH + "/" + sensor_select + "/" + label + "/" + bare_filename + '.csv',
                            dtype=np.float32, delimiter=',')
                        signal_array = signal_array[:, 1:4]
                        y_interp_axis = []
                        for i in range(signal_array.shape[1]):
                            y_interp_axis.append(np.interp(x_interp, np.arange(0, signal_array.shape[0]),
                                                           signal_array[:, i]))
                        y_interp_axis = np.array(y_interp_axis)

                        signal_array_interp = y_interp_axis.T

                    else:
                        for sensor_train in sensor_list_mech:
                            # Read csv into numpy array

                            signal_array = np.genfromtxt(
                                train_config.DATA_PATH + "/" + sensor_train + "/" + label + "/" + bare_filename + ".csv",
                                dtype=np.float32, delimiter=',')
                            signal_array = signal_array[:, 1:4]
                            y_interp_axis = []
                            for i in range(signal_array.shape[1]):
                                y_interp_axis.append(np.interp(x_interp, np.arange(0, signal_array.shape[0]),
                                                               signal_array[:, i]))
                            y_interp_axis = np.array(y_interp_axis)

                            signal_array_interp[sensor_train] = y_interp_axis.T

                    # signal_array_interp_list.append(signal_array_interp)
                    final_filename, _ = os.path.splitext(filename_img)
                    mkdir_conditional('features_fusion/' + label)
                    with open('features_fusion/' + label + '/' + final_filename + '.fusion', 'wb') as f:
                        pickle.dump(signal_array_interp, f)


                except Exception as e:
                    print(str(type(e)) + ': ' + str(e))
                    continue

        for index, item in enumerate(signal_array_interp_list):
            if sensor_select is not None and sensor_select != 'img':
                scaler.partial_fit(item)
            elif sensor_select == 'img':
                break
            else:
                for sensor in sensor_list_mech:
                    scaler[sensor].partial_fit(item[sensor])
        for index, item in enumerate(signal_array_interp_list):
            if sensor_select is not None and sensor_select in sensor_list_mech:
                signal_array_interp_list[index] = scaler.transform(item)
            elif sensor_select == 'img':
                break
            else:
                for sensor in sensor_list_mech:
                    signal_array_interp_list[index][sensor] = scaler[sensor].transform(item[sensor])
    with open('error_videos.txt', 'w') as file:
        file.write("\n".join(error_video_filenames))

    for class_index, label in enumerate(LABELS):
        for signal_index, item in enumerate(signal_array_interp_list):
            try:
                assert all(v is not None for v in item)
                if sensor_select is not None:
                    num_samples_file = len(list(range(0, item.shape[0], train_config.SEQ_STRIDE)))

                    for i in range(num_samples_file):
                        # Add to the  queue.
                        # In order to turn our discrete predictions or features into a sequence,
                        # we loop through each frame in chronological order, add it to a queue of size N,
                        # and pop off the first frame we previously added.
                        # N represents the length of our sequence that we'll pass to the RNN.
                        if sensor_select == 'img':
                            feat_vec_length = 1024
                        else:
                            feat_vec_length = 3
                        sequence = item[i:i + train_config.SEQ_LENGTH, :]
                        # Zero-padding if sequence is shorter than prescribed length
                        if sequence.shape[0] < train_config.SEQ_LENGTH:
                            sequence = np.append(sequence,
                                                 np.zeros(
                                                     [train_config.SEQ_LENGTH - sequence.shape[0],
                                                      feat_vec_length]),
                                                 axis=0)
                        # lstm_feature.append(sequence)
                        lstm_features[sensor_select].append(sequence)
                    lstm_classes.append(np.full(num_samples_file, class_index))
                else:
                    num_samples_file = len(list(range(0, item['img'].shape[0], train_config.SEQ_STRIDE)))
                    for sensor in sensor_list:
                        # lstm_feature = []

                        for i in range(num_samples_file):
                            # Add to the  queue.
                            # In order to turn our discrete predictions or features into a sequence,
                            # we loop through each frame in chronological order, add it to a queue of size N,
                            # and pop off the first frame we previously added.
                            # N represents the length of our sequence that we'll pass to the RNN.
                            if sensor == 'img':
                                feat_vec_length = 1024
                            else:
                                feat_vec_length = 3
                            sequence = item[sensor][i:i + train_config.SEQ_LENGTH, :]
                            # Zero-padding if sequence is shorter than prescribed length
                            if sequence.shape[0] < train_config.SEQ_LENGTH:
                                sequence = np.append(sequence,
                                                     np.zeros(
                                                         [train_config.SEQ_LENGTH - sequence.shape[0],
                                                          feat_vec_length]),
                                                     axis=0)
                            # lstm_feature.append(sequence)
                            lstm_features[sensor].append(sequence)
                    lstm_classes.append(np.full(num_samples_file, class_index))
            except AssertionError as e:
                print(e)
                continue
            except MemoryError as e:
                print(type(e))
                print(e)

    X_train = {}
    X_test = {}
    X_valid = {}
    MIN = {}
    RANGE = {}
    print(len(lstm_classes))
    lstm_classes = np.concatenate(lstm_classes, axis=0)
    data_indices = np.arange(lstm_classes.shape[0], dtype=np.intp)
    if sensor_select is not None and sensor_select != 'img':
        MIN[sensor_select] = scaler.data_min_
        RANGE[sensor_select] = scaler.data_range_
    elif sensor_select == 'img':
        pass
    else:
        for sensor in sensor_list_mech:
            MIN[sensor] = scaler[sensor].data_min_
            RANGE[sensor] = scaler[sensor].data_range_

    if sensor_select is not None:
        lstm_features[sensor_select] = np.array(lstm_features[sensor_select])
        print(lstm_features[sensor_select].shape[0])
        assert lstm_features[sensor_select].shape[0] == lstm_classes.shape[0]
    else:
        for sensor in sensor_list:
            lstm_features[sensor] = np.array(lstm_features[sensor])
            print(lstm_features[sensor].shape[0])

            assert lstm_features[sensor].shape[0] == lstm_classes.shape[0]
    print(lstm_classes.shape[0])
    if not crossval:
        # with open('dataset_fusion.pkl', 'wb') as f:
        #     pickle.dump((NUM_CLASSES, X_train, X_valid, X_test, y_train, y_valid, y_test, MIN, RANGE), f)
        lstm_classes = to_categorical(lstm_classes, NUM_CLASSES)
        indices_train, indices_test, _, _ = train_test_split(
            data_indices, data_indices, test_size=train_config.TEST_RATIO,
            random_state=train_config.RANDOM_STATE)
        indices_train, indices_valid, _, _ = train_test_split(
            indices_train, indices_train, test_size=train_config.VALID_RATIO,
            random_state=train_config.RANDOM_STATE)

        y_train = lstm_classes[indices_train].astype(np.float32)
        y_test = lstm_classes[indices_test].astype(np.float32)
        y_valid = lstm_classes[indices_valid].astype(np.float32)

        if sensor_select is not None and sensor_select in sensor_list:
            X_train = lstm_features[sensor_select][indices_train]
            X_test = lstm_features[sensor_select][indices_test]
            X_valid = lstm_features[sensor_select][indices_valid]

            return X_train, X_valid, X_test, y_train, y_valid, y_test

        for key in lstm_features.keys():
            X_train[key] = lstm_features[key][indices_train]
            X_test[key] = lstm_features[key][indices_test]
            X_valid[key] = lstm_features[key][indices_valid]

        return X_train, X_valid, X_test, y_train, y_valid, y_test
    else:
        if sensor_select is not None and type(sensor_select) == str:
            assert sensor_select in sensor_list
            return lstm_features[sensor_select], lstm_classes[sensor_select]
        return lstm_features, lstm_classes


def data_aded_fusion_gen(train_config, crossval=False, ):
    sensor_list = ['img', 'acc', 'gyr', 'mag']
    sensor_list_mech = ['acc', 'gyr', 'mag']
    scaler = {'acc': preprocessing.MinMaxScaler(),
              'gyr': preprocessing.MinMaxScaler(),
              'mag': preprocessing.MinMaxScaler(),
              }

    num_unique_samples = 0

    LABELS = get_labels(train_config.LABELS_FILE, train_config.DATA_PATH, train_config.SENSOR_TRAIN)

    print(LABELS)
    NUM_CLASSES = len(LABELS)

    train_config.FEAT_CACHE_PATH = 'features_fusion'

    # Data acquisition
    lstm_features = {key: [] for key in sensor_list}
    lstm_classes = {key: [] for key in sensor_list}

    with tf.gfile.FastGFile(train_config.FEAT_EXTRACTOR_NAME, 'rb') as fin:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(fin.read())
        _ = tf.import_graph_def(graph_def, name='')
    signal_array_interp_list = []
    for class_index, label in enumerate(LABELS):

        for ((_, _, files_img), (_, _, files_acc), (_, _, files_gyr), (_, _, files_mag)) \
                in zip(os.walk(train_config.DATA_PATH + "/img/" + label),
                       os.walk(train_config.DATA_PATH + "/acc/" + label),
                       os.walk(train_config.DATA_PATH + "/gyr/" + label),
                       os.walk(train_config.DATA_PATH + "/mag/" + label),
                       ):
            for (filename_img, filename_acc, filename_gyr, filename_mag) \
                    in zip(files_img, files_acc, files_gyr, files_mag):

                try:
                    # Check if there's a fusion raw data file existing
                    bare_filename, _ = os.path.splitext(filename_img)
                    if bare_filename in error_video_filenames:
                        raise Exception('Video file has errors...skipping...')
                    full_filename = train_config.FEAT_CACHE_PATH + '/' + label + '/' + bare_filename + '.fusion'
                    if os.path.exists(full_filename):
                        with open(full_filename, 'rb') as f:
                            signal_array_interp = pickle.load(f)
                            signal_array_interp_list.append(signal_array_interp)
                            signal_array_interp = {key: None for key in sensor_list}
                        continue
                except Exception as e:
                    print(type(e))
                    print(e)
                    continue

                try:
                    # If no fusion file, begin acquisition of video frames
                    assert filename_img.endswith('.mp4')
                    img_full_filename = train_config.DATA_PATH + "/img/" + label + "/" + filename_img
                    # For each frame
                    config = tf.ConfigProto()
                    config.gpu_options.allow_growth = True
                    with tf.Session(config=config) as sess:
                        with imageio.get_reader(img_full_filename, format=train_config.VIDEO_READER) as reader:
                            print("Acquiring: " + img_full_filename)
                            meta_data = reader.get_meta_data()
                            frame_period = meta_data['fps'] / meta_data['nframes']
                            x_interp = frame_period * np.arange(0, meta_data['nframes'])
                            cnn_feat_list = []

                            for frame_index, frame in enumerate(reader):
                                # if not (frame_index % int(reader._meta['fps'])):
                                # NOTE: should I add decimation here (e.g. every 10 frames?)
                                # Run Inception inference
                                pool_tensor = sess.graph.get_tensor_by_name(train_config.POOL_TENSOR_NAME)
                                # Add to queue
                                cnn_feat = sess.run(pool_tensor, {train_config.CNN_INPUT_TENSOR_NAME:
                                                                      frame})
                                cnn_feat_list.append(np.squeeze(cnn_feat))
                            signal_array_interp['img'] = np.array(cnn_feat_list)
                except imageio.core.format.CannotReadFrameError as cannot:
                    print(type(cannot))
                    print(cannot)
                    error_video_filenames.append(bare_filename)
                    continue
                except Exception as e:
                    print(e)
                    continue
                try:
                    # If no fusion file, Begin sensor acquisitions
                    for sensor_train, filename_sensor in zip(sensor_list_mech,
                                                             [filename_acc, filename_gyr, filename_mag]):
                        # Read csv into numpy array
                        assert filename_acc.endswith('.csv')
                        signal_array = np.genfromtxt(
                            train_config.DATA_PATH + "/" + sensor_train + "/" + label + "/" + filename_sensor,
                            dtype=np.float32, delimiter=',')
                        signal_array = signal_array[:, 1:4]
                        y_interp_axis = []
                        for i in range(signal_array.shape[1]):
                            y_interp_axis.append(np.interp(x_interp, np.arange(0, signal_array.shape[0]),
                                                           signal_array[:, i]))
                        y_interp_axis = np.array(y_interp_axis)

                        signal_array_interp[sensor_train] = y_interp_axis.T

                    # signal_array_interp_list.append(signal_array_interp)
                    final_filename, _ = os.path.splitext(filename_img)
                    mkdir_conditional('features_fusion/' + label)
                    with open('features_fusion/' + label + '/' + final_filename + '.fusion', 'wb') as f:
                        pickle.dump(signal_array_interp, f)


                except Exception as e:
                    print(str(type(e)) + ': ' + str(e))
                    continue

        for index, item in enumerate(signal_array_interp_list):
            for sensor in sensor_list_mech:
                scaler[sensor].partial_fit(item[sensor])
        for index, item in enumerate(signal_array_interp_list):
            for sensor in sensor_list_mech:
                signal_array_interp_list[index][sensor] = scaler[sensor].transform(item[sensor])
    with open('error_videos.txt', 'w') as file:
        file.write("\n".join(error_video_filenames))

    # train_config.SEQ_LENGTH = 50
    # train_config.SEQ_STRIDE = 1
    # sequence_tuple_list = []
    for class_index, label in enumerate(LABELS):
        for sensor in sensor_list:
            for signal_index, item in enumerate(signal_array_interp_list):
                try:
                    assert all(v is not None for v in item)
                except AssertionError as e:
                    print(e)
                    continue
                try:
                    for i in range(0, item[sensor].shape[0], train_config.SEQ_STRIDE):
                        # Add to the  queue.
                        # In order to turn our discrete predictions or features into a sequence,
                        # we loop through each frame in chronological order, add it to a queue of size N,
                        # and pop off the first frame we previously added.
                        # N represents the length of our sequence that we'll pass to the RNN.
                        if sensor == 'img':
                            feat_vec_length = 1024
                        else:
                            feat_vec_length = 3
                        sequence = item[sensor][i:i + train_config.SEQ_LENGTH, :]
                        # Zero-padding if sequence is shorter than prescribed length
                        if sequence.shape[0] < train_config.SEQ_LENGTH:
                            sequence = np.append(sequence,
                                                 np.zeros(
                                                     [train_config.SEQ_LENGTH - sequence.shape[0],
                                                      feat_vec_length]),
                                                 axis=0)
                        lstm_features[sensor].append(sequence)
                        lstm_classes[sensor].append(np.full(len(lstm_features[sensor]), class_index))
                except MemoryError as e:
                    print(type(e))
                    print(e)

    X_train = X_test = X_valid = y_train = y_valid = y_test = MIN = RANGE = dict(
        zip(sensor_list, [] * len(sensor_list)))
    for sensor in sensor_list_mech:
        MIN[sensor] = scaler[sensor].data_min_
        RANGE[sensor] = scaler[sensor].data_range_
    for sensor in sensor_list:
        print(lstm_features.keys())
        lstm_features[sensor] = np.array(lstm_features[sensor])
        if not crossval:
            lstm_classes[sensor] = to_categorical(np.array(lstm_classes[sensor]), NUM_CLASSES)

            X_train[sensor], X_test[sensor], y_train[sensor], y_test[sensor] = train_test_split(
                lstm_features[sensor], lstm_classes[sensor], test_size=train_config.TEST_RATIO,
                random_state=train_config.RANDOM_STATE)
            X_train[sensor], X_valid[sensor], y_train[sensor], y_valid[sensor] = train_test_split(
                X_train[sensor], y_train[sensor], test_size=train_config.VALID_RATIO,
                random_state=train_config.RANDOM_STATE)
        else:
            lstm_classes[sensor] = np.array(lstm_classes[sensor])
    if not crossval:
        # with open('dataset_fusion.pkl', 'wb') as f:
        #     pickle.dump((NUM_CLASSES, X_train, X_valid, X_test, y_train, y_valid, y_test, MIN, RANGE), f)

        return X_train, X_valid, X_test, y_train['img'], y_valid['img'], y_test['img']
    else:
        # with open('dataset_fusion_crossval.pkl', 'wb') as f:
        #     pickle.dump((NUM_CLASSES, lstm_features, lstm_classes, MIN, RANGE, LABELS), f)
        return lstm_features, lstm_classes


def data_mead(train_config, crossval=False):
    train_config.DATA_PATH = "dataset_mead"
    scaler = preprocessing.MinMaxScaler()
    sensor_train = train_config.SENSOR_TRAIN
    train_config.LABELS_FILE = 'har_labels_mead.txt'
    if train_config.SENSOR_TRAIN == 'img':
        train_config.FEAT_VEC_LENGTH = 1024
    else:
        train_config.FEAT_VEC_LENGTH = 3

    num_unique_samples = 0
    print('Sequence length: ' + str(train_config.SEQ_LENGTH))
    print('Sequence stride: ' + str(train_config.SEQ_STRIDE))
    LABELS = get_labels(train_config.LABELS_FILE, train_config.DATA_PATH, train_config.SENSOR_TRAIN)

    print(LABELS)
    NUM_CLASSES = len(LABELS)
    if sensor_train == 'img':
        train_config.FEAT_CACHE_PATH = "features_img_mead"

        for label in LABELS:
            mkdir_conditional(train_config.FEAT_CACHE_PATH + "/" + label)

        # Data acquisition
        lstm_features = []
        lstm_classes = []

        # with tf.Session(config=config) as sess:
        with tf.gfile.FastGFile(train_config.FEAT_EXTRACTOR_NAME, 'rb') as fin:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(fin.read())
            _ = tf.import_graph_def(graph_def, name='')
        for class_index, label in enumerate(LABELS):
            for _, _, files in os.walk(train_config.DATA_PATH + "/img/" + label):
                sess = K.get_session()
                for filename in files:
                    lstm_feature_list = []
                    # Read bottleneck cache
                    cnn_feat_filename = train_config.FEAT_CACHE_PATH + "/" + label + "/" + filename + ".feature"
                    if os.path.isfile(cnn_feat_filename):
                        with open(cnn_feat_filename, 'rb') as cnn_feat_file:
                            cnn_feats = pickle.load(cnn_feat_file)

                    else:
                        try:
                            full_filename = train_config.DATA_PATH + "/" + sensor_train + "/" + label + "/" + filename
                            # For each frame
                            with imageio.get_reader(full_filename, format=train_config.VIDEO_READER) as reader:
                                print("Acquiring: " + full_filename)
                                cnn_feats = []

                                try:
                                    for frame_index, frame in enumerate(reader):
                                        # if not (frame_index % int(reader._meta['fps'])):
                                        # NOTE: should I add decimation here (e.g. every 10 frames?)
                                        # Run Inception inference
                                        pool_tensor = sess.graph.get_tensor_by_name(train_config.POOL_TENSOR_NAME)
                                        # Add to queue
                                        cnn_feat = sess.run(pool_tensor, {train_config.CNN_INPUT_TENSOR_NAME:
                                                                              frame})
                                        cnn_feats.append(np.squeeze(cnn_feat))
                                except imageio.core.format.CannotReadFrameError:
                                    print("Erroneous last frame.")
                                    bare_filename, _ = os.path.splitext(filename)
                                    error_video_filenames.append(bare_filename)

                                cnn_feats = np.array(cnn_feats)
                                try:
                                    with open(cnn_feat_filename, 'wb') as cnn_feat_file:
                                        pickle.dump(cnn_feats, cnn_feat_file)
                                        print("Saving " + cnn_feat_filename)
                                except IOError:
                                    print("IOError occurred while saving cache")

                        except Exception as e:
                            print(str(type(e)) + ': ' + str(e))
                            continue

                    for i in range(0, cnn_feats.shape[0], train_config.SEQ_STRIDE):
                        sequence = cnn_feats[i:i + train_config.SEQ_LENGTH, :]
                        if np.size(sequence, 0) == train_config.SEQ_LENGTH:
                            lstm_feature_list.append(sequence)
                    try:
                        assert len(lstm_feature_list) > 0
                    except AssertionError:
                        print("len(lstm_feature_list) <= 0")
                        continue
                    lstm_features.extend(lstm_feature_list)
                    lstm_classes.append(np.full(len(lstm_feature_list), class_index))

        lstm_features = np.rollaxis(np.dstack(lstm_features), axis=-1)
        lstm_classes = np.array([item for sublist in lstm_classes for item in sublist])

        if not crossval:
            lstm_classes = to_categorical(lstm_classes, NUM_CLASSES)
            # Split into train and validation.
            X_train, X_test, y_train, y_test = train_test_split(
                lstm_features, lstm_classes, test_size=train_config.TEST_RATIO,
                random_state=train_config.RANDOM_STATE)
            X_train, X_valid, y_train, y_valid = train_test_split(
                X_train, y_train, test_size=train_config.VALID_RATIO,
                random_state=train_config.RANDOM_STATE)
            return X_train, X_valid, X_test, y_train, y_valid, y_test
        else:
            return lstm_features, lstm_classes, LABELS
    else:
        # Data preprocessing
        for class_index, label in enumerate(LABELS):
            for root, dirs, files in os.walk(train_config.DATA_PATH + "/csv/" + label):
                for filename in files:
                    # Read csv into numpy array
                    if filename.endswith('.csv'):
                        try:
                            signal_array = np.genfromtxt(
                                train_config.DATA_PATH + "/csv/" + label + "/" + filename,
                                dtype=np.float32,
                                delimiter=',')
                            signal_array = signal_array[:, mead_sensor_cols[sensor_train]]
                            scaler.partial_fit(signal_array)
                            num_unique_samples += len(
                                range(0, np.size(signal_array, axis=0) - train_config.SEQ_LENGTH + 1,
                                      train_config.SEQ_STRIDE))
                        except Exception as e:
                            print(e)
                            continue

        # Data acquisition
        lstm_features = []
        lstm_classes = []
        for class_index, label in enumerate(LABELS):
            # print('Class index: ' + str(class_index))
            for root, dirs, files in os.walk(train_config.DATA_PATH + "/csv/" + label):
                for filename in files:
                    # Read csv into numpy array
                    if filename.endswith('.csv'):
                        try:
                            signal_array = np.genfromtxt(
                                train_config.DATA_PATH + "/csv/" + label + "/" + filename,
                                dtype=np.float32,
                                delimiter=',')
                            signal_array = signal_array[:, mead_sensor_cols[sensor_train]]
                            signal_array = scaler.transform(signal_array)

                            for i in range(0, np.size(signal_array, axis=0), train_config.SEQ_STRIDE):
                                # Add to the queue.
                                # In order to turn our discrete predictions or features into a sequence,
                                # we loop through each frame in chronological order, add it to a queue of size N,
                                # and pop off the first frame we previously added.
                                # N represents the length of our sequence that we'll pass to the RNN.
                                sequence = signal_array[i:i + train_config.SEQ_LENGTH, :]
                                # Zero-padding if sequence is shorter than prescribed length
                                if sequence.shape[0] < train_config.SEQ_LENGTH:
                                    sequence = np.append(sequence,
                                                         np.zeros(
                                                             [train_config.SEQ_LENGTH - sequence.shape[0],
                                                              train_config.FEAT_VEC_LENGTH]),
                                                         axis=0)
                                lstm_features.append(sequence)
                                lstm_classes.append(class_index)

                        except Exception as e:
                            print(e)
                            continue

        # Numpy.
        lstm_features = np.rollaxis(np.dstack(lstm_features), axis=-1)

        MIN = scaler.data_min_
        RANGE = scaler.data_range_
        print(sensor_train + ' Min: ' + str(MIN))
        print(sensor_train + ' Range: ' + str(RANGE))
        # Split into train and validation.

        if not crossval:
            lstm_classes = to_categorical(lstm_classes, NUM_CLASSES)
            X_train, X_test, y_train, y_test = train_test_split(
                lstm_features, lstm_classes, test_size=train_config.TEST_RATIO,
                random_state=train_config.RANDOM_STATE)
            X_train, X_valid, y_train, y_valid = train_test_split(
                X_train, y_train, test_size=train_config.VALID_RATIO,
                random_state=train_config.RANDOM_STATE)
            return X_train, X_valid, X_test, y_train, y_valid, y_test, MIN, RANGE, NUM_CLASSES
        else:
            return NUM_CLASSES, lstm_features, lstm_classes, MIN, RANGE, LABELS



def train_3split(train_config, num_epochs=50, timestamp=int(time.time()), model_pretrained_filename=None, config=None,
                 dataset='dataset_aded', hyper_space=hyperspace, gpu=None, train_on_scores=False):
    # if config is not None:
    #     sess = tf.Session(config=config)
    #     K.set_session(sess)
    sensor_train = train_config.SENSOR_TRAIN
    expt_filename = train_config.EXPT_FILENAME
    train_config.NUM_EPOCHS = num_epochs
    checkpoints_dir = train_config.CHECKPOINTS_DIR
    mkdir_conditional(checkpoints_dir)

    with open(expt_filename, 'rb') as f:
        trials = pickle.load(f, encoding='bytes')
        # trials = pickle.load(f)

    best_config = space_eval(hyper_space, trials.argmin)
    if train_config.SENSOR_TRAIN in ['acc', 'gyr']:
        train_config.SEQ_LENGTH = 120
    elif train_config.SENSOR_TRAIN == 'mag':
        train_config.SEQ_LENGTH = 60
    elif train_config.SENSOR_TRAIN == 'img':
        train_config.SEQ_LENGTH = 30
    train_config.SEQ_STRIDE = 5
    train_config.BATCH_SIZE = 1024

    if train_config.DATA_PATH == "dataset_mead":
        model_weights_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + '_mead_weights.h5'
        model_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + '_mead.h5'
        model_json_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + '_mead_arch.json'
        best_hist_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + "_mead.history"
        model_combined_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + '_mead_combined.h5'
    else:
        model_weights_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + '_aded_weights.h5'
        model_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + '_aded.h5'
        model_json_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + '_aded_arch.json'
        best_hist_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + "_aded.history"
    model_combined_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + '_aded_combined.h5'
    # model.save(model_path)
    if train_config.SENSOR_TRAIN == 'fusion_full':
        dict_result, model = model_fusion_full(best_config, train_config, retrain=True, gpu=gpu)
    elif train_config.SENSOR_TRAIN == 'fusion_file':
        dict_result, model = model_fusion_from_file(best_config, train_config, crossval=False,
                                                    model_weights_file=model_weights_path,
                                                    model_combined_file=model_combined_path,
                                                    train_on_scores=train_on_scores)
    else:
        dict_result, model = model_sensor_solo(best_config, train_config, retrain=True,
                                               model_weights_file=model_weights_path,
                                               model_combined_file=model_combined_path)
    with open(model_json_path, 'w') as f:
        f.write(model.to_json())
    with open(best_hist_path, 'wb') as f:
        pickle.dump(dict_result, f)
    print("Confusion matrix (normalized): ")
    labels = get_labels(train_config.LABELS_FILE, train_config.DATA_PATH, sensor_train)
    display_confusion(dict_result['confusion_normal'], labels, sensor_train, show=True, phase='test', normalized=True)
    display_confusion(dict_result['confusion'], labels, sensor_train, show=True, phase='test', normalized=False)
    display_confusion(dict_result['confusion_train_normal'], labels, sensor_train, show=True, phase='train',
                      normalized=True)
    display_confusion(dict_result['confusion_train'], labels, sensor_train, show=True, phase='train', normalized=False)
    display_confusion(dict_result['confusion_valid_normal'], labels, sensor_train, show=True, phase='valid',
                      normalized=True)
    display_confusion(dict_result['confusion_valid'], labels, sensor_train, show=True, phase='valid', normalized=False)
    return dict_result, model_weights_path


def model_sensor_solo(hyperspace, train_config=TrainConfig(), retrain=False, model_weights_file=None,
                      model_combined_file=None,
                      gpu=None, NUM_CLASSES=8):
    # train_config = TrainConfig()
    sensor_train = train_config.SENSOR_TRAIN
    # train_config.SEQ_LENGTH = int(hyperspace['seq_length'])
    # train_config.SEQ_STRIDE = int(hyperspace['seq_stride'])
    train_config.SEQ_STRIDE = 5
    if sensor_train == 'img':
        train_config.SEQ_LENGTH = 30
        train_config.FEAT_VEC_LENGTH = 1024
    elif sensor_train in ['acc', 'gyr']:
        train_config.FEAT_VEC_LENGTH = 3
        train_config.SEQ_LENGTH = 120
    elif sensor_train == 'mag':
        train_config.FEAT_VEC_LENGTH = 3
        train_config.SEQ_LENGTH = 60

    train_config.BATCH_SIZE = 1024
    # train_config.DATA_PATH = "dataset_aded/"

    timestamp = int(time.time())
    print(hyperspace)
    # if gpu is not None:
    #     with tf.device('/gpu:' + str(gpu)):

    ########################## MODEL ###############################

    inputs = Input(shape=[train_config.SEQ_LENGTH, train_config.FEAT_VEC_LENGTH],
                   name="input_" + train_config.SENSOR_TRAIN)
    # net = Lambda(lambda x: (x - MIN) / RANGE)(inputs)

    if sensor_train == 'img':
        if train_config.DATA_PATH == 'dataset_aded':
            X_train, X_valid, X_test, y_train, y_valid, y_test, NUM_CLASSES = data_aded(train_config)
        elif train_config.DATA_PATH == 'dataset_mead':
            X_train, X_valid, X_test, y_train, y_valid, y_test, NUM_CLASSES = data_mead(train_config)
        if hyperspace['lstm_1']:
            net = LSTM(int(hyperspace['lstm_1']['lstm_1_num_cells']), name='lstm_1', return_sequences=True,
                       # recurrent_regularizer=regularizers.l2(hyperspace['lstm_1']['lstm_1_l2_strength']),
                       # kernel_regularizer=regularizers.l2(hyperspace['lstm_1']['lstm_1_l2_strength'])
                       )(inputs)

            if hyperspace['lstm_1']['lstm_2']:
                lstm_1 = hyperspace['lstm_1']
                net = LSTM(int(lstm_1['lstm_2']['lstm_2_num_cells']), name='lstm_2', return_sequences=True,
                           # recurrent_regularizer=regularizers.l2(lstm_1['lstm_2']['lstm_2_l2_strength']),
                           # kernel_regularizer=regularizers.l2(lstm_1['lstm_2']['lstm_2_l2_strength'])
                           )(net)

                if lstm_1['lstm_2']['lstm_3']:
                    lstm_2 = lstm_1['lstm_2']
                    net = LSTM(int(lstm_2['lstm_3']['lstm_3_num_cells']), name='lstm_3', return_sequences=True,
                               # recurrent_regularizer=regularizers.l2(lstm_2['lstm_3']['lstm_3_l2_strength']),
                               # kernel_regularizer=regularizers.l2(lstm_2['lstm_3']['lstm_3_l2_strength'])
                               )(net)

            net = LSTM(int(hyperspace['lstm_last']['lstm_last_num_cells']),
                       # recurrent_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                       # kernel_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                       name='lstm_last')(net)
        else:
            net = LSTM(int(hyperspace['lstm_last']['lstm_last_num_cells']),
                       # recurrent_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                       # kernel_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                       name='lstm_last')(inputs)

    else:
        if train_config.DATA_PATH == 'dataset_aded':
            X_train, X_valid, X_test, y_train, y_valid, y_test, MIN, RANGE, NUM_CLASSES = data_aded(train_config)
        elif train_config.DATA_PATH == 'dataset_mead':
            X_train, X_valid, X_test, y_train, y_valid, y_test, MIN, RANGE, NUM_CLASSES = data_mead(train_config)

        net = Conv1D(int(hyperspace['cnn_1']['cnn_1_num_filters']), int(hyperspace['cnn_1']['cnn_1_size_filters']),
                     # kernel_regularizer=regularizers.l2(hyperspace['cnn_1']['cnn_1_l2_strength'])
                     )(inputs)
        if hyperspace['cnn_1']['cnn_2']:
            cnn_1 = hyperspace['cnn_1']
            net = Conv1D(int(cnn_1['cnn_2']['cnn_2_num_filters']), int(cnn_1['cnn_2']['cnn_2_size_filters']),
                         # kernel_regularizer=regularizers.l2(cnn_1['cnn_2']['cnn_2_l2_strength'])
                         )(net)

            if cnn_1['cnn_2']['cnn_3']:
                cnn_2 = cnn_1['cnn_2']
                net = Conv1D(int(cnn_2['cnn_3']['cnn_3_num_filters']), int(cnn_2['cnn_3']['cnn_3_size_filters']),
                             # kernel_regularizer=regularizers.l2(cnn_2['cnn_3']['cnn_3_l2_strength'])
                             )(net)

                if cnn_2['cnn_3']['cnn_4']:
                    cnn_3 = cnn_2['cnn_3']
                    net = Conv1D(int(cnn_3['cnn_4']['cnn_4_num_filters']), int(cnn_3['cnn_4']['cnn_4_size_filters']),
                                 # kernel_regularizer=regularizers.l2(cnn_3['cnn_4']['cnn_4_l2_strength']))
                                 )(net)

        if hyperspace['lstm_1']:
            net = LSTM(int(hyperspace['lstm_1']['lstm_1_num_cells']), name='lstm_1', return_sequences=True,
                       # recurrent_regularizer=regularizers.l2(hyperspace['lstm_1']['lstm_1_l2_strength']),
                       # kernel_regularizer=regularizers.l2(hyperspace['lstm_1']['lstm_1_l2_strength'])
                       )(net)

            if hyperspace['lstm_1']['lstm_2']:
                lstm_1 = hyperspace['lstm_1']
                net = LSTM(int(lstm_1['lstm_2']['lstm_2_num_cells']), name='lstm_2', return_sequences=True,
                           # recurrent_regularizer=regularizers.l2(lstm_1['lstm_2']['lstm_2_l2_strength']),
                           # kernel_regularizer=regularizers.l2(lstm_1['lstm_2']['lstm_2_l2_strength'])
                           )(net)

                if lstm_1['lstm_2']['lstm_3']:
                    lstm_2 = lstm_1['lstm_2']
                    net = LSTM(int(lstm_2['lstm_3']['lstm_3_num_cells']), name='lstm_3', return_sequences=True,
                               # recurrent_regularizer=regularizers.l2(lstm_2['lstm_3']['lstm_3_l2_strength']),
                               # kernel_regularizer=regularizers.l2(lstm_2['lstm_3']['lstm_3_l2_strength'])
                               )(net)

        net = LSTM(int(hyperspace['lstm_last']['lstm_last_num_cells']),
                   # recurrent_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                   # kernel_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                   name='lstm_last')(net)

        # if hyperspace['dropout']:
    net = Dropout(hyperspace['dropout_prob'])(net)
    predictions = Dense(NUM_CLASSES, activation='softmax', name='pred_' + train_config.SENSOR_TRAIN,
                        # kernel_regularizer=regularizers.l2(hyperspace['dense_l2_strength'])
                        )(net)

    model_sensor = Model(inputs=[inputs], outputs=[predictions])

    dict_result = {'model_definition': model_sensor.to_json()}

    if MULTI_GPU:
        par = Parallelizer()
        model_sensor_par = par.transform(model_sensor)
        # model_sensor_par = to_multi_gpu(model_sensor, N_GPUS)
    else:
        model_sensor_par = model_sensor
    model_sensor_par.compile(optimizer='adam', loss='categorical_crossentropy',
                             metrics=['accuracy', 'categorical_accuracy', 'top_k_categorical_accuracy', metrics.mae,
                                      metrics.mse])

    # earlystop = keras.callbacks.EarlyStopping('val_loss', min_delta=0.001, patience=train_config.NUM_EPOCHS / 8,
    #                                           verbose=1, mode='auto')
    earlystop = callbacks.EarlyStopping(monitor='val_acc', min_delta=0, patience=4, verbose=1, mode='max')
    time_start = time.time()
    model_history = model_sensor_par.fit(X_train, y_train, validation_data=(X_valid, y_valid),
                                         batch_size=train_config.BATCH_SIZE,
                                         epochs=train_config.NUM_EPOCHS,
                                         shuffle=False,
                                         callbacks=[earlystop]
                                         )
    time_end = time.time()
    # Test the model on the test set
    metrics_list = model_sensor_par.evaluate(X_test, y_test, batch_size=train_config.BATCH_SIZE)
    metrics_list_train = model_sensor_par.evaluate(X_train, y_train, batch_size=train_config.BATCH_SIZE)
    metrics_list_valid = model_sensor_par.evaluate(X_valid, y_valid, batch_size=train_config.BATCH_SIZE)

    y_pred = model_sensor_par.predict(X_test, verbose=1)
    y_pred_train = model_sensor_par.predict(X_train, verbose=1)
    y_pred_valid = model_sensor_par.predict(X_valid, verbose=1)
    print('Current timestamp: ' + str(timestamp))
    print("Confusion matrix: ")
    confusion = confusion_matrix(np.argmax(y_test, axis=1), np.argmax(y_pred, axis=1))
    confusion_train = confusion_matrix(np.argmax(y_train, axis=1), np.argmax(y_pred_train, axis=1))
    confusion_valid = confusion_matrix(np.argmax(y_valid, axis=1), np.argmax(y_pred_valid, axis=1))
    # Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into
    # account. If 'weighted': Calculate metrics for each label, and find their average, weighted by support (the
    # number of true instances for each label). This alters 'macro' to account for label imbalance; it can result
    #  in an F-score that is not between precision and recall.
    precision, recall, f1, support = precision_recall_fscore_support(np.argmax(y_test, axis=1),
                                                                     np.argmax(y_pred, axis=1), average='weighted')
    # Calculate metrics for each label, and find their average, weighted by support (the number of true instances
    #  for each label).
    mAP = average_precision_score(y_test, y_pred, average='weighted')
    print('Non-normalized train confusion matrix:')
    print(confusion_train)
    print('Non-normalized validation confusion matrix:')
    print(confusion_valid)
    print('Non-normalized test confusion matrix:')
    print(confusion)

    print('Normalized training confusion matrix: ')
    confusion_normal_train = confusion_train.astype('float') / confusion_train.sum(axis=1)[:, np.newaxis]
    print(confusion_normal_train)
    print('Normalized validation confusion matrix: ')
    confusion_normal_valid = confusion_valid.astype('float') / confusion_valid.sum(axis=1)[:, np.newaxis]
    print(confusion_normal_valid)
    print('Normalized  test confusion matrix: ')
    confusion_normal = confusion.astype('float') / confusion.sum(axis=1)[:, np.newaxis]
    print(confusion_normal)
    print("Precision: " + str(precision))
    print("Recall: " + str(recall))
    print("F1-score: " + str(f1))
    print("Support: " + str(support))

    print("Mean average precision: " + str(mAP))
    # fig = display_confusion(confusion_normal,LABELS)
    # K.clear_session()
    print('Test set accuracy:', metrics_list[1])

    dict_result.update(
        {'loss': metrics_list[0],
         'status': STATUS_OK,
         'test_accuracy': metrics_list[1],
         'test_precision': precision,
         'test_recall': recall,
         'test_f1': f1,
         'confusion': confusion,
         'confusion_normal': confusion_normal,
         'confusion_train': confusion_train,
         'confusion_train_normal': confusion_normal_train,
         'confusion_valid': confusion_valid,
         'confusion_valid_normal': confusion_normal_valid,
         'support': support,
         'mAP': mAP,
         # 'model_history': model_history.history,
         'sensor': sensor_train,
         'timestamp': timestamp,
         'training_time': time_end - time_start,
         'other_metrics': metrics_list
         })

    if sensor_train != 'img':
        dict_result['data_min'] = MIN
        dict_result['data_range'] = RANGE
    # plot_model(model_sensor_par, to_file='model.png')
    if model_weights_file is not None:
        if MULTI_GPU:
            par.save_model(model_weights_file, model_sensor_par)
        else:
            model_sensor_par.trainable = False
            frozen_graph = freeze_session(K.get_session(), output_names=[model_sensor_par.output.op.name])
            with open("results_" + sensor_train + "/chkpts/" + sensor_train + "_graph.pb", 'wb') as f:
                f.write(frozen_graph.SerializeToString())
            # par.save_model(model_file, model_sensor_par)
            # saver = tf.train.Saver(max_to_keep=1)
            # saver.save(K.get_session(),"results_acc/tf/acc_model")
            # model_sensor_par._make_predict_function()
            # graph = tf.get_default_graph()
            # with graph.as_default():
            if model_weights_file is not None:
                model_sensor_par.save_weights(model_weights_file)
            if model_combined_file is not None:
                model_sensor_par.save(model_combined_file)
                # model_sensor.save_weights(model_file)
    if retrain:
        return dict_result, model_sensor
    return dict_result


def model_sensor_solo_from_fusion(hyperspace, train_config=TrainConfig()):
    sensor_train = train_config.SENSOR_TRAIN
    train_config.SEQ_LENGTH = int(hyperspace['seq_length'])
    train_config.SEQ_STRIDE = int(hyperspace['seq_stride'])
    train_config.BATCH_SIZE = int(hyperspace['batch_size'])

    # train_config.DATA_PATH = "dataset_aded/"
    if train_config.SENSOR_TRAIN == 'img':
        train_config.FEAT_VEC_LENGTH = 1024
    else:
        train_config.FEAT_VEC_LENGTH = 3

    timestamp = int(time.time())
    print(hyperspace)

    ########################## MODEL ###############################

    inputs = Input(shape=[train_config.SEQ_LENGTH, train_config.FEAT_VEC_LENGTH],
                   name="input_" + train_config.SENSOR_TRAIN)
    # net = Lambda(lambda x: (x - MIN) / RANGE)(inputs)
    if train_config.DATA_PATH == 'dataset_aded':
        X_train, X_valid, X_test, y_train, y_valid, y_test = data_aded_fusion(train_config, sensor_select=sensor_train)
    elif train_config.DATA_PATH == 'dataset_mead':
        X_train, X_valid, X_test, y_train, y_valid, y_test = data_mead(train_config)

    NUM_CLASSES = y_train.shape[1]
    if sensor_train == 'img':

        if hyperspace['lstm_1']:
            net = LSTM(int(hyperspace['lstm_1']['lstm_1_num_cells']), name='lstm_1', return_sequences=True,
                       recurrent_regularizer=regularizers.l2(hyperspace['lstm_1']['lstm_1_l2_strength']),
                       kernel_regularizer=regularizers.l2(hyperspace['lstm_1']['lstm_1_l2_strength']))(inputs)

            if hyperspace['lstm_1']['lstm_2']:
                lstm_1 = hyperspace['lstm_1']
                net = LSTM(int(lstm_1['lstm_2']['lstm_2_num_cells']), name='lstm_2', return_sequences=True,
                           recurrent_regularizer=regularizers.l2(lstm_1['lstm_2']['lstm_2_l2_strength']),
                           kernel_regularizer=regularizers.l2(lstm_1['lstm_2']['lstm_2_l2_strength'])
                           )(net)

                if lstm_1['lstm_2']['lstm_3']:
                    lstm_2 = lstm_1['lstm_2']
                    net = LSTM(int(lstm_2['lstm_3']['lstm_3_num_cells']), name='lstm_3', return_sequences=True,
                               recurrent_regularizer=regularizers.l2(lstm_2['lstm_3']['lstm_3_l2_strength']),
                               kernel_regularizer=regularizers.l2(lstm_2['lstm_3']['lstm_3_l2_strength'])
                               )(net)

            net = LSTM(int(hyperspace['lstm_last']['lstm_last_num_cells']),
                       recurrent_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                       kernel_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                       name='lstm_last')(net)
        else:
            net = LSTM(int(hyperspace['lstm_last']['lstm_last_num_cells']),
                       recurrent_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                       kernel_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                       name='lstm_last')(inputs)

        if hyperspace['dropout']:
            net = Dropout(hyperspace['dropout']['dropout_prob'])(net)
        predictions = Dense(NUM_CLASSES, activation='softmax', name='pred_' + train_config.SENSOR_TRAIN,
                            kernel_regularizer=regularizers.l2(hyperspace['dense_l2_strength']))(net)
    else:
        if train_config.DATA_PATH == 'dataset_aded':
            NUM_CLASSES, X_train, X_valid, X_test, y_train, y_valid, y_test, MIN, RANGE = data_aded(train_config)
        elif train_config.DATA_PATH == 'dataset_mead':
            NUM_CLASSES, X_train, X_valid, X_test, y_train, y_valid, y_test, MIN, RANGE = data_mead(train_config)

        net = Conv1D(int(hyperspace['cnn_1']['cnn_1_num_filters']), int(hyperspace['cnn_1']['cnn_1_size_filters']),
                     kernel_regularizer=regularizers.l2(hyperspace['cnn_1']['cnn_1_l2_strength']))(
            inputs)
        if hyperspace['cnn_1']['cnn_2']:
            cnn_1 = hyperspace['cnn_1']
            net = Conv1D(int(cnn_1['cnn_2']['cnn_2_num_filters']), int(cnn_1['cnn_2']['cnn_2_size_filters']),
                         kernel_regularizer=regularizers.l2(cnn_1['cnn_2']['cnn_2_l2_strength']))(
                net)

            if cnn_1['cnn_2']['cnn_3']:
                cnn_2 = cnn_1['cnn_2']
                net = Conv1D(int(cnn_2['cnn_3']['cnn_3_num_filters']), int(cnn_2['cnn_3']['cnn_3_size_filters']),
                             kernel_regularizer=regularizers.l2(cnn_2['cnn_3']['cnn_3_l2_strength']))(
                    net)

                if cnn_2['cnn_3']['cnn_4']:
                    cnn_3 = cnn_2['cnn_3']
                    net = Conv1D(int(cnn_3['cnn_4']['cnn_4_num_filters']), int(cnn_3['cnn_4']['cnn_4_size_filters']),
                                 kernel_regularizer=regularizers.l2(cnn_3['cnn_4']['cnn_4_l2_strength']))(
                        net)

        if hyperspace['lstm_1']:
            net = LSTM(int(hyperspace['lstm_1']['lstm_1_num_cells']), name='lstm_1', return_sequences=True,
                       recurrent_regularizer=regularizers.l2(hyperspace['lstm_1']['lstm_1_l2_strength']),
                       kernel_regularizer=regularizers.l2(hyperspace['lstm_1']['lstm_1_l2_strength']))(net)

            if hyperspace['lstm_1']['lstm_2']:
                lstm_1 = hyperspace['lstm_1']
                net = LSTM(int(lstm_1['lstm_2']['lstm_2_num_cells']), name='lstm_2', return_sequences=True,
                           recurrent_regularizer=regularizers.l2(lstm_1['lstm_2']['lstm_2_l2_strength']),
                           kernel_regularizer=regularizers.l2(lstm_1['lstm_2']['lstm_2_l2_strength'])
                           )(net)

                if lstm_1['lstm_2']['lstm_3']:
                    lstm_2 = lstm_1['lstm_2']
                    net = LSTM(int(lstm_2['lstm_3']['lstm_3_num_cells']), name='lstm_3', return_sequences=True,
                               recurrent_regularizer=regularizers.l2(lstm_2['lstm_3']['lstm_3_l2_strength']),
                               kernel_regularizer=regularizers.l2(lstm_2['lstm_3']['lstm_3_l2_strength'])
                               )(net)

        net = LSTM(int(hyperspace['lstm_last']['lstm_last_num_cells']),
                   recurrent_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                   kernel_regularizer=regularizers.l2(hyperspace['lstm_last']['lstm_last_l2_strength']),
                   name='lstm_last')(net)

        if hyperspace['dropout']:
            net = Dropout(hyperspace['dropout']['dropout_prob'])(net)
        predictions = Dense(NUM_CLASSES, activation='softmax', name='pred_' + train_config.SENSOR_TRAIN,
                            kernel_regularizer=regularizers.l2(hyperspace['dense_l2_strength']))(net)

    model_sensor = Model(inputs=inputs, outputs=predictions)
    # MULTI_GPU = False
    if MULTI_GPU:
        model_sensor = to_multi_gpu(model_sensor, N_GPUS)
    model_sensor.compile(optimizer='adam', loss='categorical_crossentropy',
                         metrics=['accuracy', 'categorical_accuracy', 'top_k_categorical_accuracy', metrics.mae,
                                  metrics.mse])

    # earlystop = keras.callbacks.EarlyStopping('val_loss', min_delta=0.001, patience=train_config.NUM_EPOCHS / 8,
    #                                           verbose=1, mode='auto')
    earlystop = callbacks.EarlyStopping(monitor='val_loss', min_delta=0.001, patience=0, verbose=1, mode='auto')
    time_start = time.time()
    try:
        model_history = model_sensor.fit(X_train, y_train, validation_data=(X_valid, y_valid),
                                         batch_size=hyperspace['batch_size'],
                                         shuffle=False,
                                         callbacks=[earlystop],
                                         epochs=train_config.NUM_EPOCHS)
    except ZeroDivisionError:
        print("Can't fit. Zero division error happens somewhere in training.")
        return

    time_end = time.time()
    # Test the model on the test set
    try:
        metrics_list = model_sensor.evaluate(X_test, y_test, batch_size=train_config.BATCH_SIZE)
    except ZeroDivisionError:
        print("Can't evaluate. Zero division error happens somewhere in the testing.")
        return

    print('Current timestamp: ' + str(timestamp))
    print("Confusion matrix: ")
    y_pred = model_sensor.predict(X_test, verbose=1)
    confusion = confusion_matrix(np.argmax(y_test, axis=1), np.argmax(y_pred, axis=1))
    # Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into
    # account. If 'weighted': Calculate metrics for each label, and find their average, weighted by support (the
    # number of true instances for each label). This alters 'macro' to account for label imbalance; it can result
    #  in an F-score that is not between precision and recall.
    precision, recall, f1, support = precision_recall_fscore_support(np.argmax(y_test, axis=1),
                                                                     np.argmax(y_pred, axis=1), average='weighted')
    # Calculate metrics for each label, and find their average, weighted by support (the number of true instances
    #  for each label).
    mAP = average_precision_score(y_test, y_pred, average='weighted')
    print('Non-normalized confusion matrix:')
    print(confusion)
    print('Normalized confusion matrix: ')
    try:
        confusion_normal = confusion.astype('float') / confusion.sum(axis=1)[:, np.newaxis]
        print(confusion_normal)
    except ZeroDivisionError:
        print("Confusion matrix can't be computed. Zero division error.")

    print("Precision: " + str(precision))
    print("Recall: " + str(recall))
    print("F1-score: " + str(f1))
    print("Support: " + str(support))

    print("Mean average precision: " + str(mAP))
    # fig = display_confusion(confusion_normal,LABELS)
    K.clear_session()
    print('Test set accuracy:', metrics_list[1])
    dict_result = {'loss': metrics_list[0],
                   'status': STATUS_OK,
                   'test_accuracy': metrics_list[1],
                   'test_precision': precision,
                   'test_recall': recall,
                   'test_f1': f1,
                   'confusion': confusion,
                   'confusion_normal': confusion_normal,
                   'support': support,
                   'mAP': mAP,
                   'model_definition': model_sensor.to_json(),
                   'model_history': model_history.history,
                   'sensor': sensor_train,
                   'timestamp': timestamp,
                   'training_time': time_end - time_start,
                   'other_metrics': metrics_list
                   }
    if sensor_train != 'img':
        dict_result['data_min'] = MIN
        dict_result['data_range'] = RANGE
    return dict_result


def run_hyperopt(sensor_train, num_trials, num_epochs=10, timestamp_experiment=int(time.time()), gpu=None,
                 train_config=TrainConfig()):
    EXPERIMENTS_DIR = 'results_' + sensor_train + '/expts/'
    trials = Trials()

    train_config.RANDOM_STATE = 42
    train_config.SENSOR_TRAIN = sensor_train
    train_config.NUM_EPOCHS = num_epochs
    train_config.SEQ_STRIDE = 5
    if sensor_train == 'mag':
        train_config.SEQ_LENGTH = 60
    # hyperspace['seq_length'] = hp.quniform('seq_length', 25, 75, 1)
    # elif sensor_train == 'img':
    #     hyperspace['seq_length'] = hp.quniform('seq_length', 5, 15, 1)
    #     hyperspace['seq_stride'] = hp.quniform('seq_stride', 1, 5, 1)
    if sensor_train not in ['fusion_file', 'fusion_full']:
        best = fmin(fn=partial(model_sensor_solo, train_config=train_config, gpu=gpu),
                    space=hyperspace,
                    algo=tpe.suggest,
                    max_evals=num_trials,
                    trials=trials,
                    rstate=np.random.RandomState(train_config.RANDOM_STATE))

        best_eval = space_eval(hyperspace, best)
    elif sensor_train == 'fusion_full':
        best = fmin(fn=partial(model_fusion_full, train_config=train_config, gpu=gpu),
                    space=hyperspace_fusion_full,
                    algo=tpe.suggest,
                    max_evals=num_trials,
                    trials=trials,
                    rstate=np.random.RandomState(train_config.RANDOM_STATE))

        best_eval = space_eval(hyperspace_fusion_file, best)
    elif sensor_train == 'fusion_file':

        best = fmin(fn=partial(model_fusion_from_file, train_config=train_config, gpu=gpu),
                    space=hyperspace_fusion_file,
                    algo=tpe.suggest,
                    max_evals=num_trials,
                    trials=trials,
                    rstate=np.random.RandomState(train_config.RANDOM_STATE))

        best_eval = space_eval(hyperspace_fusion_file, best)

    print(best_eval)
    print(trials.best_trial)

    # Save to experiment file
    mkdir_conditional(EXPERIMENTS_DIR)
    if train_config.NUM_EPOCHS == 1:
        trial_file = EXPERIMENTS_DIR + sensor_train + '_hyperopt_' + str(timestamp_experiment) + '-codetest.expt'
    else:
        trial_file = EXPERIMENTS_DIR + sensor_train + '_hyperopt_' + str(timestamp_experiment) + '.expt'
    with open(trial_file, 'wb') as f:
        pickle.dump(trials, f)
        print('Saved to: ' + trial_file)
    return trial_file


def pop_layer(model):
    if not model.outputs:
        raise Exception('Model cannot be popped: model is empty.')

    model.layers.pop()
    if not model.layers:
        model.outputs = []
        model.inbound_nodes = []
        model.outbound_nodes = []
    else:
        model.layers[-1].outbound_nodes = []
        model.outputs = [model.layers[-1].output]
    model.built = False
    return model


def cross_validate(train_config, k_folds=5, num_epochs=50, timestamp=int(time.time()), old_model=False,
                   dataset='dataset_aded', config=None, crossval=True, gpu=None, NUM_CLASSES=8):
    if config is not None:
        sess = tf.Session(config=config)
        K.set_session(sess)
    sensor_train = train_config.SENSOR_TRAIN
    expt_filename = train_config.EXPT_FILENAME

    checkpoints_dir = train_config.CHECKPOINTS_DIR
    mkdir_conditional(checkpoints_dir)
    kfold = StratifiedKFold(n_splits=k_folds, shuffle=True, random_state=train_config.RANDOM_STATE)
    with open(expt_filename, 'rb') as f:
        trials = pickle.load(f, encoding='bytes')
    best_trial = trials.best_trial
    best_config = space_eval(hyperspace, trials.argmin)
    if train_config.SENSOR_TRAIN in ['acc', 'gyr']:
        train_config.SEQ_LENGTH = 120
    elif train_config.SENSOR_TRAIN == 'mag':
        train_config.SEQ_LENGTH = 60
    elif train_config.SENSOR_TRAIN == 'img':
        train_config.SEQ_LENGTH = 3
    train_config.SEQ_STRIDE = 5
    train_config.BATCH_SIZE = 128

    crossval_scores_models = []
    if sensor_train == 'img':
        if dataset == 'dataset_mead':
            X, y, LABELS = data_mead(train_config, crossval=crossval)
        else:
            X, y, LABELS = data_aded(train_config, crossval=crossval)
    else:
        if dataset == 'dataset_mead':
            X, y, MIN, RANGE, LABELS = data_mead(train_config, crossval=crossval)
        else:
            X, y, MIN, RANGE, LABELS = data_aded(train_config, crossval=crossval)

    if crossval:
        for train, test in kfold.split(X, y):
            y_cat = to_categorical(y, NUM_CLASSES)
            if type(old_model) == Model:
                old_model.trainable = False
                # Extract LSTM output tensor
                lstm_output = old_model.layers[-2].output
                reg = old_model.layers[-1].kernel_regularizer
                new_dense = Dense(NUM_CLASSES, activation='softmax', name='pred_' + train_config.SENSOR_TRAIN,
                                  kernel_regularizer=reg)(lstm_output)
                model = Model(inputs=[old_model.input], outputs=[new_dense])
                if MULTI_GPU:
                    model = to_multi_gpu(model, N_GPUS)
                model.compile(optimizer='adam', loss='categorical_crossentropy',
                              metrics=['accuracy', 'categorical_accuracy', 'top_k_categorical_accuracy', metrics.mae,
                                       metrics.mse])

            else:
                model = rebuild_model(model_string=best_trial['result']['model_definition'])

            history = model.fit(X[train], y_cat[train], epochs=num_epochs, batch_size=train_config.BATCH_SIZE,
                                validation_data=(X[test], y_cat[test]))
            scores = model.evaluate(X[test], y_cat[test])
            y_pred = model.predict(X[test], verbose=1)
            confusion = confusion_matrix(np.argmax(y_cat[test], axis=1), np.argmax(y_pred, axis=1))
            precision, recall, f1, support = precision_recall_fscore_support(np.argmax(y_cat[test], axis=1),
                                                                             np.argmax(y_pred, axis=1),
                                                                             average='weighted')
            mAP = average_precision_score(y_cat[test], y_pred, average='weighted')
    # else:

    confusion_normal = confusion.astype('float') / confusion.sum(axis=1)[:, np.newaxis]
    print(confusion_normal)
    crossval_scores_models.append({'model': model,
                                   'scores': scores,
                                   'history': history.history,
                                   'confusion': confusion,
                                   'confusion_normal': confusion_normal,
                                   'precision': precision,
                                   'recall': recall,
                                   'f1': f1,
                                   'support': support,
                                   'mAP': mAP})
    print("Test Accuracy: " + str(scores[1] * 100) + "%")
    best_index = np.argmax(np.array([item['scores'][1] for item in crossval_scores_models]))
    assert type(best_index) == np.int64
    best_index = int(best_index)
    best_model = crossval_scores_models[best_index]['model']
    best_history = crossval_scores_models[best_index]['history']
    best_confusion_normal = crossval_scores_models[best_index]['confusion_normal']
    best_confusion = crossval_scores_models[best_index]['confusion']
    best_confusion_train_normal = crossval_scores_models[best_index]['confusion_normal']
    best_confusion_normal = crossval_scores_models[best_index]['confusion_normal']
    best_confusion_normal = crossval_scores_models[best_index]['confusion_normal']
    best_confusion_normal = crossval_scores_models[best_index]['confusion_normal']
    print("BEST MODEL")
    print(best_model.summary())
    print("Accuracy: " + str(crossval_scores_models[best_index]['scores'][1] * 100) + "%")
    print("Precision: " + str(crossval_scores_models[best_index]['precision'] * 100) + "%")
    print("Recall: " + str(crossval_scores_models[best_index]['recall'] * 100) + "%")
    print("F1-score: " + str(crossval_scores_models[best_index]['f1'] * 100) + "%")
    # print("Support: " + str(crossval_scores_models[best_index]['f1'] * 100) + "\%" )


    if type(old_model) == Model:
        best_model_path = checkpoints_dir + 'best_' + sensor_train + '_mead.h5'
        best_hist_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + "_mead.history"
    else:
        best_model_path = checkpoints_dir + 'best_' + sensor_train + '_aded.h5'
        best_hist_path = checkpoints_dir + 'best_' + sensor_train + '_' + str(timestamp) + "_aded.history"
    best_model.save(best_model_path)
    with open(best_hist_path, 'wb') as f:
        for item in crossval_scores_models:
            item.pop('model', None)
        crossval_scores_models.sort(reverse=True, key=lambda x: x['scores'][1])
        pickle.dump(crossval_scores_models, f)
    print("Confusion matrix (normalized): ")
    labels = get_labels(train_config.LABELS_FILE, train_config.DATA_PATH, sensor_train)
    display_confusion(best_confusion_normal, labels, sensor_train, True)
    return best_model, best_history, best_model_path


def expt_runner(sensor_train, num_trials=20, num_epochs_hyperopt=10, k_folds=5, num_epochs_cross=50,
                dataset='dataset_aded', config=None, gpu=None, train_config=TrainConfig(),
                labels_file='har_labels.txt'):
    if config is None:
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
    # if dataset == 'dataset_mead':
    #     train_config.LABELS_FILE = 'har_labels_mead.txt'
    # else:
    #     train_config.LABELS_FILE = 'har_labels.txt'
    train_config.LABELS_FILE = labels_file
    sess = tf.Session(config=config)
    K.set_session(sess)

    timestamp = time.time()
    # NOTE: this train_config is different from the one used internally in run_hyperopt!
    # train_config = TrainConfig()

    LOGGER_DIR = "results_" + sensor_train + "/logs/"
    mkdir_conditional(LOGGER_DIR)
    log = logger_init(filename=LOGGER_DIR + sensor_train + str(int(timestamp)) + ".log")
    sys.stdout = log

    train_config.DATA_PATH = dataset

    # Hyperparameter optimization

    train_config.SENSOR_TRAIN = sensor_train
    if sensor_train == 'mag':
        train_config.SEQ_LENGTH = 60
    elif sensor_train == 'img':
        train_config.SEQ_LENGTH = 30

    if sensor_train in ['fusion_file']:

        trials_filename = run_hyperopt(sensor_train, num_trials, num_epochs_hyperopt, timestamp_experiment=timestamp,
                                       train_config=train_config)
        K.clear_session()
        train_config.CHECKPOINTS_DIR = "results_" + sensor_train + "/chkpts/"
        train_config.EXPT_FILENAME = trials_filename
        train_config.SENSOR_TRAIN = 'fusion_file'
        best_model, best_model_path = train_3split(train_config,
                                                   num_epochs=num_epochs_cross,
                                                   dataset=dataset, hyper_space=hyperspace_fusion_file, gpu=gpu,
                                                   )
        print("Best model saved at: " + best_model_path)
        K.clear_session()
    elif sensor_train in ['fusion_full']:
        train_config.DATA_PATH = dataset
        trials_filename = run_hyperopt(sensor_train, num_trials, num_epochs_hyperopt, timestamp_experiment=timestamp,
                                       gpu=gpu)
        K.clear_session()
        train_config.CHECKPOINTS_DIR = "results_" + sensor_train + "/chkpts/"
        train_config.EXPT_FILENAME = trials_filename
        best_model, best_model_path = train_3split(train_config,
                                                   num_epochs=num_epochs_cross,
                                                   dataset=dataset, hyper_space=hyperspace_fusion_full, gpu=gpu)
        print("Best model saved at: " + best_model_path)
        K.clear_session()
        # X_train, X_valid, X_test, y_train, y_valid, y_test = data_aded_fusion(train_config)

    if sensor_train not in ['fusion_full', 'fusion_file']:
        # Cross-validation
        trials_filename = run_hyperopt(sensor_train, num_trials, num_epochs_hyperopt, train_config=train_config,
                                       timestamp_experiment=timestamp,
                                       gpu=gpu)
        K.clear_session()
        train_config.DATA_PATH = dataset
        train_config.CHECKPOINTS_DIR = "results_" + sensor_train + "/chkpts/"
        train_config.EXPT_FILENAME = trials_filename

        best_model, best_model_path = train_3split(train_config,
                                                   num_epochs=num_epochs_cross,
                                                   dataset=dataset, hyper_space=hyperspace, gpu=gpu)
        K.clear_session()
        print("Best model saved at: " + best_model_path)

    input("Press Enter to continue...")


def data_mead_fusion(train_config):
    return None, None, None, None, None, None, None, None, None
    pass


def model_fusion_from_file(hyperspace, train_config, crossval=False, expt_img=None, expt_acc=None, expt_gyr=None,
                           expt_mag=None, gpu=None, NUM_CLASSES=8, model_weights_file=None, model_combined_file=None,
                           train_on_scores=False):
    timestamp = int(time.time())
    X_train = {}
    X_test = {}
    X_valid = {}
    y_train = {}
    y_test = {}
    y_valid = {}
    MIN = {}
    RANGE = {}
    if train_config.DATA_PATH == 'dataset_aded':
        train_config.SEQ_STRIDE = 5
        train_config.SEQ_LENGTH = 30
        train_config.SENSOR_TRAIN = 'img'
        # X_train['img'], X_valid['img'], X_test['img'], y_train['img'], y_valid['img'], y_test['img'], NUM_CLASSES \
        #     = data_aded(train_config)
        _, _, _, y_train['img'], y_valid['img'], y_test['img'], NUM_CLASSES \
            = data_aded(train_config)
        train_config.SEQ_STRIDE = 20
        train_config.SENSOR_TRAIN = 'acc'
        train_config.SEQ_LENGTH = 120
        # X_train['acc'], X_valid['acc'], X_test['acc'], y_train['acc'], y_valid['acc'], y_test['acc'], MIN['acc'], RANGE[
        #     'acc'], _ \
        #     = data_aded(train_config)
        _, _, _, y_train['acc'], y_valid['acc'], y_test['acc'], MIN['acc'], RANGE[
            'acc'], _ \
            = data_aded(train_config)
        train_config.SEQ_STRIDE = 20
        train_config.SENSOR_TRAIN = 'gyr'
        train_config.SEQ_LENGTH = 120
        # X_train['gyr'], X_valid['gyr'], X_test['gyr'], y_train['gyr'], y_valid['gyr'], y_test['gyr'], MIN['gyr'], RANGE[
        #     'gyr'], _ \
        #     = data_aded(train_config)
        _, _, _, y_train['gyr'], y_valid['gyr'], y_test['gyr'], MIN['gyr'], RANGE[
            'gyr'], _ \
            = data_aded(train_config)
        train_config.SEQ_STRIDE = 10
        train_config.SENSOR_TRAIN = 'mag'
        train_config.SEQ_LENGTH = 60
        # X_train['mag'], X_valid['mag'], X_test['mag'], y_train['mag'], y_valid['mag'], y_test['mag'], MIN['mag'], RANGE[
        #     'mag'], _ \
        #     = data_aded(train_config)
        _, _, _, y_train['mag'], y_valid['mag'], y_test['mag'], MIN['mag'], RANGE[
            'mag'], _ \
            = data_aded(train_config)
        # X_train, X_valid, X_test, y_train, y_valid, y_test = data_aded_fusion(train_config)
        min_num_samples_train = np.min(np.array(
            [y_train['img'].shape[0], y_train['acc'].shape[0], y_train['gyr'].shape[0], y_train['mag'].shape[0]]))
        min_num_samples_test = np.min(np.array(
            [y_test['img'].shape[0], y_test['acc'].shape[0], y_test['gyr'].shape[0], y_test['mag'].shape[0]]))
        min_num_samples_valid = np.min(np.array(
            [y_valid['img'].shape[0], y_valid['acc'].shape[0], y_valid['gyr'].shape[0], y_valid['mag'].shape[0]]))
        for key in X_train.keys():
            X_train[key] = X_train[key][:min_num_samples_train]
        for key in X_valid.keys():
            X_valid[key] = X_valid[key][:min_num_samples_valid]
        for key in X_test.keys():
            X_test[key] = X_test[key][:min_num_samples_test]

        y_train = y_train['img'][:min_num_samples_train]
        y_test = y_test['img'][:min_num_samples_test]
        y_valid = y_valid['img'][:min_num_samples_valid]




    else:
        NUM_CLASSES, X_train, X_valid, X_test, y_train, y_valid, y_test, MIN, RANGE = data_mead_fusion(train_config)

    with K.name_scope("img"):
        # with open(train_config.IMG_CHKPT_FILENAME + ".json", 'r') as f:
        #     model_img = model_from_json(f.read().replace('\n', ''))
        # model_img.load_weights(train_config.IMG_CHKPT_FILENAME + ".h5")
        model_img = load_model(train_config.IMG_CHKPT_FILENAME)
        model_img.trainable = False

        for i in range(len(model_img.layers)):
            model_img.layers[i].name = "img/" + model_img.layers[i].name
            model_img.layers[i].trainable = False

    with K.name_scope("acc"):

        model_acc = load_model(train_config.ACC_CHKPT_FILENAME)
        model_acc.trainable = False

        for i in range(len(model_acc.layers)):
            model_acc.layers[i].name = "acc/" + model_acc.layers[i].name
            model_acc.layers[i].trainable = False

    with K.name_scope("gyr"):

        model_gyr = load_model(train_config.GYR_CHKPT_FILENAME)
        model_gyr.trainable = False

        for i in range(len(model_gyr.layers)):
            model_gyr.layers[i].name = "gyr/" + model_gyr.layers[i].name
            model_gyr.layers[i].trainable = False

    with K.name_scope("mag"):

        model_mag = load_model(train_config.MAG_CHKPT_FILENAME)
        model_mag.trainable = False

        for i in range(len(model_mag.layers)):
            model_mag.layers[i].name = "mag/" + model_mag.layers[i].name
            model_mag.layers[i].trainable = False

    # model_img_output.name = "Img"
    if train_on_scores:
        concat = Concatenate()([model_img.output, model_acc.output, model_gyr.output, model_mag.output])
    else:
        concat = Concatenate()([model_img.layers[-2].output, model_acc.layers[-2].output, model_gyr.layers[-2].output,
                                model_mag.layers[-2].output])

    if hyperspace['dense_1']:
        concat = Dense(int(hyperspace['dense_1']['dense_1_num_units']), activation='relu',
                       # kernel_regularizer=regularizers.l2(hyperspace['dense_1']['dense_1_l2_strength'])
                       )(concat)
    concat = Dropout(hyperspace['dropout_prob'])(concat)
    pred_fusion = Dense(NUM_CLASSES, activation='softmax', name='pred_fusion',
                        # kernel_regularizer=regularizers.l2(hyperspace['dense_last']['dense_last_l2_strength'])
                        )(concat)

    model_fusion = Model(inputs=[model_img.input, model_acc.input, model_gyr.input, model_mag.input],
                         outputs=[pred_fusion])
    # if MULTI_GPU:
    #     model_fusion_par = to_multi_gpu(model_fusion, N_GPUS)
    # else:
    #     model_fusion_par = model_fusion
    model_fusion.compile(optimizer='adam', loss='categorical_crossentropy',
                             metrics=['accuracy', 'categorical_accuracy', 'top_k_categorical_accuracy', metrics.mae,
                                      metrics.mse])
    time_start = time.time()
    earlystop = callbacks.EarlyStopping(monitor='val_loss', min_delta=0.0000000001, patience=10, verbose=1, mode='auto')

    model_history = model_fusion.fit([X_train['img'], X_train['acc'], X_train['gyr'], X_train['mag']],
                                         [y_train],
                                         validation_data=(
                                             [X_valid['img'], X_valid['acc'], X_valid['gyr'], X_valid['mag']],
                                             [y_valid]),
                                         batch_size=1024,
                                         epochs=train_config.NUM_EPOCHS,
                                         shuffle=False,
                                         callbacks=[earlystop]
                                         )
    time_end = time.time()
    # Test the model on the test set
    metrics_list = model_fusion.evaluate([X_test['img'], X_test['acc'], X_test['gyr'], X_test['mag']], y_test,
                                             batch_size=train_config.BATCH_SIZE)
    y_pred = model_fusion.predict([X_test['img'], X_test['acc'], X_test['gyr'], X_test['mag']], verbose=1)
    y_pred_train = model_fusion.predict([X_train['img'], X_train['acc'], X_train['gyr'], X_train['mag']], verbose=1)
    y_pred_valid = model_fusion.predict([X_valid['img'], X_valid['acc'], X_valid['gyr'], X_valid['mag']], verbose=1)
    print('Current timestamp: ' + str(timestamp))
    print("Confusion matrix: ")
    confusion = confusion_matrix(np.argmax(y_test, axis=1), np.argmax(y_pred, axis=1))
    confusion_train = confusion_matrix(np.argmax(y_train, axis=1), np.argmax(y_pred_train, axis=1))
    confusion_valid = confusion_matrix(np.argmax(y_valid, axis=1), np.argmax(y_pred_valid, axis=1))
    print('Current timestamp: ' + str(timestamp))
    print("Confusion matrix: ")

    confusion = confusion_matrix(np.argmax(y_test, axis=1), np.argmax(y_pred, axis=1))

    # Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into
    # account. If 'weighted': Calculate metrics for each label, and find their average, weighted by support (the
    # number of true instances for each label). This alters ‘macro’ to account for label imbalance; it can result
    #  in an F-score that is not between precision and recall.
    precision, recall, f1, support = precision_recall_fscore_support(np.argmax(y_test, axis=1),
                                                                     np.argmax(y_pred, axis=1), average='weighted')
    # Calculate metrics for each label, and find their average, weighted by support (the number of true instances
    #  for each label).
    AP = average_precision_score(y_test, y_pred, average='weighted')
    print('Non-normalized confusion matrix:')
    print(confusion)
    print('Normalized confusion matrix: ')
    confusion_normal = confusion.astype('float') / confusion.sum(axis=1)[:, np.newaxis]
    print(confusion_normal)
    print('Normalized training confusion matrix: ')
    confusion_normal_train = confusion_train.astype('float') / confusion_train.sum(axis=1)[:, np.newaxis]
    print(confusion_normal_train)
    print('Normalized validation confusion matrix: ')
    confusion_normal_valid = confusion_valid.astype('float') / confusion_valid.sum(axis=1)[:, np.newaxis]
    print(confusion_normal_valid)
    print("Precision: " + str(precision))
    print("Recall: " + str(recall))
    print("F1-score: " + str(f1))
    print("Support: " + str(support))
    print("Average precision: " + str(AP))
    # fig = display_confusion(confusion_normal,LABELS)
    # model_fusion.save('fusion_model.h5')

    print('Test set accuracy:', metrics_list[1])
    dict_result = {'loss': metrics_list[0],
                   'status': STATUS_OK,
                   'test_accuracy': metrics_list[1],
                   'test_precision': precision,
                   'test_recall': recall,
                   'test_f1': f1,
                   'confusion': confusion,
                   'confusion_normal': confusion_normal,
                   'confusion_train': confusion_train,
                   'confusion_train_normal': confusion_normal_train,
                   'confusion_valid': confusion_valid,
                   'confusion_valid_normal': confusion_normal_valid,
                   'support': support,
                   'AP': AP,
                   'model_definition': model_fusion.to_json(),
                   # 'model_history': model_history.history,
                   'timestamp': timestamp,
                   'training_time': time_end - time_start,
                   'other_metrics': metrics_list,
                   'min': MIN,
                   'range': RANGE
                   }
    sess = K.get_session()
    model_fusion.summary()
    # display_nodes(sess.graph.as_graph_def().node)
    if crossval:
        if model_weights_file is not None:
            model_fusion.save_weights(model_weights_file)
        if model_combined_file is not None:
            model_fusion.save(model_combined_file)
        K.clear_session()
        return dict_result, model_fusion
    else:
        model_fusion.trainable = False
        K.set_learning_phase(0)

        for i in range(len(model_fusion.layers)):
            model_fusion.layers[i].trainable = False
        # sess = K.get_session()

        display_nodes(sess.graph.as_graph_def().node)
        frozen_graph = freeze_session(sess, output_names=[model_fusion.output.op.name, model_img.output.op.name,
                                                          model_acc.output.op.name, model_gyr.output.op.name,
                                                          model_mag.output.op.name])
        gd = frozen_graph
        nodes_with_switch_op = [x for x in gd.node if x.op.lower().find('switch') != -1]
        # # Rerouting Switch Ops
        for n in [x for x in gd.node]:
            ints = [i for i in n.input]
            endswith1 = [ii for ii in ints if ii.endswith(':1')]  # and 'Switch' in ii]
            if len(endswith1) > 0:
                for index, inn in enumerate(n.input):
                    if inn in endswith1:
                        new_input = inn[:-2]
                        n.input.remove(inn)
                        n.input.insert(index, new_input)
        for n in nodes_with_switch_op:
            n.op = 'Identity'
            n.input.pop()

        display_nodes(frozen_graph.node)

        with open("results_fusion_file/chkpts/fusion_graph.pb", 'wb') as f:
            f.write(frozen_graph.SerializeToString())

        if model_weights_file is not None:
            # par.save_model(model_file, model_fusion)
            # saver = tf.train.Saver(max_to_keep=1)
            # saver.save(K.get_session(),"results_acc/tf/acc_model")
            # model_fusion._make_predict_function()
            # graph = tf.get_default_graph()
            # with graph.as_default():

            model_fusion.save_weights(model_weights_file)
        if model_combined_file is not None:
            model_fusion.save(model_combined_file)
                # model_fusion.save_weights(model_file)
        K.clear_session()
        return dict_result, model_fusion


def model_fusion_full(hyperspace, train_config, crossval=False, NUM_CLASSES=8, retrain=False, gpu=None):
    timestamp = int(time.time())
    train_config.SEQ_LENGTH = 30
    train_config.SEQ_STRIDE = 5

    if train_config.DATA_PATH == 'dataset_aded':
        if not crossval:
            X_train, X_valid, X_test, y_train, y_valid, y_test = data_aded_fusion(train_config, crossval=crossval)
        else:
            X, y = data_aded_fusion(train_config, crossval=crossval)
    else:
        X_train, X_valid, X_test, y_train, y_valid, y_test = data_mead_fusion(train_config)

    input_img = Input(shape=(train_config.SEQ_LENGTH, 1024), name='input_img')
    inputs_sensor = {'acc': Input(shape=(train_config.SEQ_LENGTH, 3), name='input_acc'),
                     'gyr': Input(shape=(train_config.SEQ_LENGTH, 3), name='input_gyr'),
                     'mag': Input(shape=(train_config.SEQ_LENGTH, 3), name='input_mag')
                     }

    net = {}
    if gpu is not None:
        with tf.device('/gpu:' + str(gpu)):
            if hyperspace['img']['img_lstm_1']:
                net['img'] = LSTM(int(hyperspace['img']['img_lstm_1']['img_lstm_1_num_cells']), name='img_lstm_1',
                                  return_sequences=True,
                                  # recurrent_regularizer=regularizers.l2(
                                  #     hyperspace['img']['img_lstm_1']['img_lstm_1_l2_strength']),
                                  # kernel_regularizer=regularizers.l2(
                                  #     hyperspace['img']['img_lstm_1']['img_lstm_1_l2_strength'])
                                  )(
                    input_img)

                if hyperspace['img']['img_lstm_1']['img_lstm_2']:
                    lstm_1 = hyperspace['img']['img_lstm_1']
                    net['img'] = LSTM(int(lstm_1['img_lstm_2']['img_lstm_2_num_cells']), name='img_lstm_2',
                                      return_sequences=True,
                                      # recurrent_regularizer=regularizers.l2(lstm_1['img_lstm_2']['img_lstm_2_l2_strength']),
                                      # kernel_regularizer=regularizers.l2(lstm_1['img_lstm_2']['img_lstm_2_l2_strength'])
                                      )(net['img'])

                    if lstm_1['img_lstm_2']['img_lstm_3']:
                        lstm_2 = lstm_1['img_lstm_2']
                        net['img'] = LSTM(int(lstm_2['img_lstm_3']['img_lstm_3_num_cells']), name='img_lstm_3',
                                          return_sequences=True,
                                          # recurrent_regularizer=regularizers.l2(lstm_2['img_lstm_3']['img_lstm_3_l2_strength']),
                                          # kernel_regularizer=regularizers.l2(lstm_2['img_lstm_3']['img_lstm_3_l2_strength'])
                                          )(net['img'])

                net['img'] = LSTM(int(hyperspace['img']['img_lstm_last']['img_lstm_last_num_cells']),
                                  # recurrent_regularizer=regularizers.l2(
                                  #     hyperspace['img']['img_lstm_last']['img_lstm_last_l2_strength']),
                                  # kernel_regularizer=regularizers.l2(
                                  #     hyperspace['img']['img_lstm_last']['img_lstm_last_l2_strength']),
                                  name='img_lstm_last')(net['img'])
            else:
                net['img'] = LSTM(int(hyperspace['img']['img_lstm_last']['img_lstm_last_num_cells']),
                                  # recurrent_regularizer=regularizers.l2(
                                  #     hyperspace['img']['img_lstm_last']['img_lstm_last_l2_strength']),
                                  # kernel_regularizer=regularizers.l2(
                                  #     hyperspace['img']['img_lstm_last']['img_lstm_last_l2_strength']),
                                  name='img_lstm_last')(input_img)

            # if hyperspace['img']['img_dropout']:
            # net['img'] = Dropout(hyperspace['img']['img_dropout']['img_dropout_prob'])(net['img'])
            # model_img_output = Dense(NUM_CLASSES, activation='softmax', name='pred_img',
            #                          kernel_regularizer=regularizers.l2(hyperspace['img']['img_dense_l2_strength']))(net['img'])
            model_img_output = net['img']
            model_sensor_output = {}
            for sensor in ['acc', 'gyr', 'mag']:
                net[sensor] = Conv1D(int(hyperspace[sensor][sensor + '_cnn_1'][sensor + '_cnn_1_num_filters']),
                                     int(hyperspace[sensor][sensor + '_cnn_1'][sensor + '_cnn_1_size_filters']),
                                     # kernel_regularizer=regularizers.l2(
                                     #     hyperspace[sensor][sensor + '_cnn_1'][sensor + '_cnn_1_l2_strength'])
                                     )(
                    inputs_sensor[sensor])
                if hyperspace[sensor][sensor + '_cnn_1'][sensor + '_cnn_2']:
                    cnn_1 = hyperspace[sensor][sensor + '_cnn_1']
                    net[sensor] = Conv1D(int(cnn_1[sensor + '_cnn_2'][sensor + '_cnn_2_num_filters']),
                                         int(cnn_1[sensor + '_cnn_2'][sensor + '_cnn_2_size_filters']),
                                         # kernel_regularizer=regularizers.l2(
                                         #     cnn_1[sensor + '_cnn_2'][sensor + '_cnn_2_l2_strength'])
                                         )(
                        net[sensor])

                    if cnn_1[sensor + '_cnn_2'][sensor + '_cnn_3']:
                        cnn_2 = cnn_1[sensor + '_cnn_2']
                        net[sensor] = Conv1D(int(cnn_2[sensor + '_cnn_3'][sensor + '_cnn_3_num_filters']),
                                             int(cnn_2[sensor + '_cnn_3'][sensor + '_cnn_3_size_filters']),
                                             # kernel_regularizer=regularizers.l2(
                                             #     cnn_2[sensor + '_cnn_3'][sensor + '_cnn_3_l2_strength'])
                                             )(net[sensor])

                        if cnn_2[sensor + '_cnn_3'][sensor + '_cnn_4']:
                            cnn_3 = cnn_2[sensor + '_cnn_3']
                            net[sensor] = Conv1D(int(cnn_3[sensor + '_cnn_4'][sensor + '_cnn_4_num_filters']),
                                                 int(cnn_3[sensor + '_cnn_4'][sensor + '_cnn_4_size_filters']),
                                                 # kernel_regularizer=regularizers.l2(
                                                 #     cnn_3[sensor + '_cnn_4'][sensor + '_cnn_4_l2_strength'])
                                                 )(
                                net[sensor])

                if hyperspace[sensor][sensor + '_lstm_1']:
                    net[sensor] = LSTM(int(hyperspace[sensor][sensor + '_lstm_1'][sensor + '_lstm_1_num_cells']),
                                       name=sensor + 'lstm_1',
                                       return_sequences=True,
                                       # recurrent_regularizer=regularizers.l2(
                                       #     hyperspace[sensor][sensor + '_lstm_1'][sensor + '_lstm_1_l2_strength']),
                                       # kernel_regularizer=regularizers.l2(
                                       #     hyperspace[sensor][sensor + '_lstm_1'][sensor + '_lstm_1_l2_strength'])
                                       )(
                        net[sensor])

                    if hyperspace[sensor][sensor + '_lstm_1'][sensor + '_lstm_2']:
                        lstm_1 = hyperspace[sensor][sensor + '_lstm_1']
                        net[sensor] = LSTM(int(lstm_1[sensor + '_lstm_2'][sensor + '_lstm_2_num_cells']),
                                           name=sensor + 'lstm_2', return_sequences=True,
                                           # recurrent_regularizer=regularizers.l2(
                                           #     lstm_1[sensor + '_lstm_2'][sensor + '_lstm_2_l2_strength']),
                                           # kernel_regularizer=regularizers.l2(
                                           #     lstm_1[sensor + '_lstm_2'][sensor + '_lstm_2_l2_strength'])
                                           )(net[sensor])

                        if lstm_1[sensor + '_lstm_2'][sensor + '_lstm_3']:
                            lstm_2 = lstm_1[sensor + '_lstm_2']
                            net[sensor] = LSTM(int(lstm_2[sensor + '_lstm_3'][sensor + '_lstm_3_num_cells']),
                                               name=sensor + 'lstm_3', return_sequences=True,
                                               # recurrent_regularizer=regularizers.l2(
                                               #     lstm_2[sensor + '_lstm_3'][sensor + '_lstm_3_l2_strength']),
                                               # kernel_regularizer=regularizers.l2(
                                               #     lstm_2[sensor + '_lstm_3'][sensor + '_lstm_3_l2_strength'])
                                               )(net[sensor])

                net[sensor] = LSTM(int(hyperspace[sensor][sensor + '_lstm_last'][sensor + '_lstm_last_num_cells']),
                                   # recurrent_regularizer=regularizers.l2(
                                   #     hyperspace[sensor][sensor + '_lstm_last'][
                                   #         sensor + '_lstm_last_l2_strength']),
                                   # kernel_regularizer=regularizers.l2(
                                   #     hyperspace[sensor][sensor + '_lstm_last'][
                                   #         sensor + '_lstm_last_l2_strength']),
                                   name=sensor + 'lstm_last')(net[sensor])

                # if hyperspace[sensor][sensor + '_dropout']:
                #     net[sensor] = Dropout(hyperspace[sensor][sensor + '_dropout'][sensor + '_dropout_prob'])(
                #         net[sensor])

                # model_sensor_output[sensor] = Dense(NUM_CLASSES, activation='softmax', name='pred_' + sensor,
                #                                     kernel_regularizer=regularizers.l2(
                #                                         hyperspace[sensor][sensor + '_dense_l2_strength']))(
                #     net[sensor])
                model_sensor_output[sensor] = net[sensor]
            concat = Concatenate()(
                [model_img_output, model_sensor_output['acc'], model_sensor_output['gyr'],
                 model_sensor_output['mag'], ])
            if hyperspace['dense_1']:
                concat = Dense(int(hyperspace['dense_1']['dense_1_num_units']), activation='relu',
                               # kernel_regularizer=regularizers.l2(hyperspace['dense_1']['dense_1_l2_strength'])
                               )(
                    concat)
            pred_fusion = Dense(NUM_CLASSES, activation='softmax', name='pred_fusion',
                                # kernel_regularizer=regularizers.l2(
                                #     hyperspace['dense_last']['dense_last_l2_strength'])
                                )(concat)

            model_fusion = Model(inputs=[input_img, inputs_sensor['acc'], inputs_sensor['gyr'], inputs_sensor['mag']],
                                 outputs=pred_fusion)
            if MULTI_GPU:
                par = Parallelizer()
                model_fusion_par = par.transform(model_fusion)
                # model_fusion = to_multi_gpu(model_fusion, N_GPUS)
            else:
                model_fusion_par = model_fusion
            model_fusion_par.compile(optimizer='adam', loss='categorical_crossentropy',
                                     metrics=['accuracy', 'categorical_accuracy', 'top_k_categorical_accuracy',
                                              metrics.mae,
                                              metrics.mse])
            time_start = time.time()

            if not crossval:
                model_history = model_fusion_par.fit([X_train['img'], X_train['acc'], X_train['gyr'], X_train['mag']],
                                                     y_train,
                                                     validation_data=(
                                                         [X_valid['img'], X_valid['acc'], X_valid['gyr'],
                                                          X_valid['mag']],
                                                         y_valid),
                                                     batch_size=hyperspace['batch_size'],
                                                     epochs=train_config.NUM_EPOCHS)
            else:
                k_folds = 5
                model_history = []
                kfold = StratifiedKFold(n_splits=k_folds, shuffle=True, random_state=train_config.RANDOM_STATE)
                print(type(X))
                print(X['img'].shape)
                for train, test in kfold.split(X['img'], y):
                    y_cat = to_categorical(y, NUM_CLASSES)
                    history = model_fusion_par.fit(
                        [X['img'][train, :, :], X['acc'][train, :, :], X['gyr'][train, :, :], X['mag'][train, :, :]],
                        y_cat[train],
                        validation_data=(
                            [X['img'][test, :, :], X['acc'][test, :, :], X['gyr'][test, :, :],
                             X['mag'][test, :, :]],
                            y_cat[test]),
                        batch_size=hyperspace['batch_size'],
                        epochs=train_config.NUM_EPOCHS)
                    model_history.append(history)
                    metrics_list = model_fusion.evaluate(
                        [X['img'][test, :, :], X['acc'][test, :, :], X['gyr'][test, :, :], X['mag'][test, :, :]],
                        y_cat[test], batch_size=train_config.BATCH_SIZE)
            time_end = time.time()
            metrics_list = model_fusion_par.evaluate([X_test['img'], X_test['acc'], X_test['gyr'], X_test['mag']],
                                                     y_test,
                                                     batch_size=hyperspace['batch_size'])

            # Test the model on the test set
            print('Current timestamp: ' + str(timestamp))
            print("Confusion matrix: ")
            y_pred = model_fusion_par.predict([X_test['img'], X_test['acc'], X_test['gyr'], X_test['mag']], verbose=1)
    try:
        confusion = confusion_matrix(np.argmax(y_test, axis=1), np.argmax(y_pred, axis=1))
    except ZeroDivisionError:
        print("Zero division error!")
    # Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into
    # account. If 'weighted': Calculate metrics for each label, and find their average, weighted by support (the
    # number of true instances for each label). This alters ‘macro’ to account for label imbalance; it can result
    #  in an F-score that is not between precision and recall.
    precision, recall, f1, support = precision_recall_fscore_support(np.argmax(y_test, axis=1),
                                                                     np.argmax(y_pred, axis=1), average='weighted')
    # Calculate metrics for each label, and find their average, weighted by support (the number of true instances
    #  for each label).
    AP = average_precision_score(y_test, y_pred, average='weighted')
    print('Non-normalized confusion matrix:')
    print(confusion)
    print('Normalized confusion matrix: ')
    confusion_normal = confusion.astype('float') / confusion.sum(axis=1)[:, np.newaxis]
    print(confusion_normal)
    print("Precision: " + str(precision))
    print("Recall: " + str(recall))
    print("F1-score: " + str(f1))
    print("Support: " + str(support))
    print("Average precision: " + str(AP))
    # fig = display_confusion(confusion_normal,LABELS)
    K.clear_session()
    print('Test set accuracy:', metrics_list[1])
    dict_result = {'loss': metrics_list[0],
                   'status': STATUS_OK,
                   'test_accuracy': metrics_list[1],
                   'test_precision': precision,
                   'test_recall': recall,
                   'test_f1': f1,
                   'confusion': confusion,
                   'confusion_normal': confusion_normal,
                   'support': support,
                   'AP': AP,
                   'model_definition': model_fusion_par.to_json(),
                   'model_history': model_history.history,
                   'timestamp': timestamp,
                   'training_time': time_end - time_start,
                   'other_metrics': metrics_list,
                   # 'min': MIN,
                   # 'range': RANGE
                   }
    if retrain:
        return dict_result, model_fusion_par
    else:
        return dict_result


def plot_final_stats(filename, sensor, labels_filename):
    _, file_ext = os.path.splitext(filename)
    assert file_ext == '.history'
    _, ext = os.path.splitext(labels_filename)
    assert ext == '.txt'
    LABELS = get_labels(labels_filename)
    hist = pickle.load(open(filename, 'rb'))
    # hist.sort(key=lambda x: x['scores'][0])
    acc_train = hist['model_history']['acc']
    acc_val = hist['model_history']['val_acc']
    loss_train = hist['model_history']['loss']
    loss_val = hist['model_history']['val_loss']
    confusion = hist['confusion_normal']
    precision = hist['test_precision']
    recall = hist['test_recall']
    f1 = hist['test_f1']
    # AP = hist[0]['AP']

    print("\n" + sensor_fullname[sensor] + " stats:")
    print("Accuracy (final): " + str(hist['test_accuracy']))
    print("Precision: " + str(precision))
    print("Recall: " + str(recall))
    print("F1-score: " + str(f1))
    # print("Average Precision: " + str(AP))
    print("Loss (final): " + str(hist['loss']))

    plt.figure()
    acc_line_val = plt.plot(np.arange(len(acc_val)), np.array(acc_val), 'g', label='Validation')
    acc_line_train = plt.plot(np.arange(len(acc_train)), np.array(acc_train), 'b', label='Training')
    plt.legend(loc='upper left')
    plt.title(sensor_fullname[sensor] + " Network Accuracies per Epoch")
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')

    plt.figure()
    loss_line_val = plt.plot(np.arange(len(loss_val)), np.array(loss_val), 'g', label='Validation')
    loss_line_train = plt.plot(np.arange(len(loss_train)), np.array(loss_train), 'b', label='Training')
    plt.legend(loc='upper left')
    plt.title(sensor_fullname[sensor] + " Network Losses per Epoch")
    plt.xlabel('Epochs')
    plt.ylabel('Categorical Cross-entropy')

    width = 12
    height = 12
    plt.figure(figsize=(width, height))
    # fig = plt.figure(figsize=(width, height))
    tick_marks = np.arange(len(LABELS))
    plt.xticks(tick_marks, LABELS, rotation=45)
    plt.yticks(tick_marks, LABELS)
    plt.imshow(
        confusion,
        interpolation='nearest',
        cmap=plt.cm.Blues,
        # figure=fig
    )
    plt.title(sensor_fullname[sensor] + " Normalized Confusion Matrix")
    thresh = confusion.max() / 2.
    for i, j in itertools.product(range(confusion.shape[0]), range(confusion.shape[1])):
        plt.text(j, i, "{0:.2f}".format(confusion[i, j]),
                 horizontalalignment="center",
                 color="white" if confusion[i, j] > thresh else "black")
    plt.colorbar()
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    plt.show()


def show_best_hyperparams(filename, hyper_space=hyperspace):
    with open(filename, 'rb') as f:
        trials = pickle.load(f)
    best_config = space_eval(hyper_space, trials.argmin)
    print(best_config)
